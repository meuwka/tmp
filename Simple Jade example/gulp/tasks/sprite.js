var gulp = require('gulp');
var gulpif = require('gulp-if');
var sprity = require('sprity');

/*gulp.task('sprites', function () {
  return gulp.src('app/images/sprite/src/*.png')
    .pipe(sprite({
      name: 'sprite',
      style: 'sprite.scss',
      cssPath: '../images/sprite/',
      processor: 'scss'
    }))
    .pipe(gulpif('*.png', gulp.dest('app/images/sprite/'), gulp.dest('app/styles/components/')))
});*/


// generate sprite.png and _sprite.scss
gulp.task('sprites', function () {
  return sprity.src({
    src: 'app/images/sprite/src/*.png',
    style: '../images/sprite/sprite.scss'
  })
      .pipe(gulpif('*.png', gulp.dest('app/images/sprite/'), gulp.dest('app/styles/components/')))
});
