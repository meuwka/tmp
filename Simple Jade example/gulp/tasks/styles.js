'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');

var reload = require('./browserSync.js').reload;
var handleError = require('./../utils/handleError.js');


// Complie scss using libsass

gulp.task('styles', function () {
  return gulp.src('app/styles/main.scss')
    .pipe(sass())
    .pipe(autoprefixer({browsers: ['last 2 version', 'ie 10']}))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream:true}));
});
