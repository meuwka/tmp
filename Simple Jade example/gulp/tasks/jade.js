'use strict';

var gulp = require('gulp');
var jade = require('gulp-jade');
//var data = require('gulp-data');
var plumber  = require('gulp-plumber');
//var fs = require('fs');
var extend = require('gulp-extend');

var reload = require('./browserSync.js').reload;
var handleError = require('./../utils/handleError.js');

// Compile jade to html

gulp.task('jade', function() {
  return gulp.src('app/views/*.jade')
    .pipe(plumber(handleError))
    .pipe(jade({
      pretty: true,
      compileDebug: false
    }))
    .pipe(gulp.dest('.tmp/'))
    .pipe(reload({stream:true}));
});
