﻿/******************************************************
*This code was origionally written by: Paul Schoneveld
*@ ClickPopMedia
*Paul doesn't rightly care what you do with this code,
*but he thinks it would be nice if you remembered him
*in your heart while using it.  Also, he would be happy
*if a link to http://www.clickpopmedia.com/ showed up
*on your site.  But, none of that is required nore could
*it be enforced.
********************************************************/

/*Unlike the VolControl class I don't want to update this
LIVE, so I only update when I'm done dragging.*/
package controls {
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.external.ExternalInterface;
	import com.greensock.*;
	
	public class SeekControl extends MovieClip {
		private var bounds:Rectangle;
		public var percent:Number;
		public var seeking:Boolean;
		
		public function SeekControl() {
			
			seeking = false;
			percent = 0;
			bounds = new Rectangle(1, 5, 701, 0);
			
			/*seekHit_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandle, false, 0, true);
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDown, false, 0, true);
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_UP, endDragUp, false, 0, true);
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_OUT, endDrag, false, 0, true);*/
			
			seekHit_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandle, false, 0, true);
			
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDown, false, 0, true);
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_UP, endDragUp, false, 0, true);
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_OUT, outHandle, false, 0, true);
		}
		
		private function dragHandle(event:MouseEvent):void {
			seeking = true;
			seekHandle_mc.startDrag(true, bounds);
		}
		
		private function dragHandleDown(event:MouseEvent):void {
			seekHandle_mc.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			seeking = true;
			seekHandle_mc.startDrag(true, bounds);
			dispatchEvent(new Event("seek_down"));
		}
		private function endDragUp(event:MouseEvent):void {
			seekHandle_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			moveComplete();
		}
		private function outHandle(event:MouseEvent):void {
			if (seekHandle_mc.hasEventListener(MouseEvent.MOUSE_MOVE))
			{
				seekHandle_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
				moveComplete();
			}
		}
		private function moveHandle(event:MouseEvent):void {
			fillBar_mc.x = seekHandle_mc.x - fillBar_mc.width;
		}
		private function moveComplete():void {
			seeking = false;
			seekHandle_mc.stopDrag();
			fillBar_mc.x = seekHandle_mc.x - fillBar_mc.width;
			percent = seekHandle_mc.x/701;
			dispatchEvent(new Event("seek_change"));
			dispatchEvent(new Event("seek_up"));
		}
		
		public function moveSeekPos(perc:Number):void {
			//fillBar_mc.x = (fillBar_mc.width * perc) - fillBar_mc.width;
			TweenMax.to(fillBar_mc, 0, {x:(fillBar_mc.width * perc) - fillBar_mc.width});
			if(!seeking){
				TweenMax.to(seekHandle_mc, 0, {x:701 * perc});
				}
			//seekHandle_mc.x = 701 * perc;}
		}
	}
}