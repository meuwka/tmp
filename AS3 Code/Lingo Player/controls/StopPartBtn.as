﻿package controls {
	import flash.display.MovieClip;
	
	public class StopPartBtn extends MovieClip {
		
		public function StopPartBtn() {
			this.stop();
			this.mouseChildren = false;
		}
	}
}