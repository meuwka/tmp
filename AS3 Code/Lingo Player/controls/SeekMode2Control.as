﻿package controls {
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.external.ExternalInterface;
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class SeekMode2Control extends MovieClip {
		private var bounds:Rectangle;
		public var seeking:Boolean;
		public var seekingLoop:Boolean;
		public var widthChannel:int;
		
		public function SeekMode2Control() {
			seeking = false;
			seekingLoop = false;
			widthChannel = 584;
			bounds = new Rectangle(59, 5, widthChannel, 0);
			
			seekHitLoop_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDownLoop, false, 0, true);
			
			seekHandleLeft_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDownLeft, false, 0, true);
			seekHandleLeft_mc.addEventListener(MouseEvent.MOUSE_UP, endDragUpLeft, false, 0, true);
			seekHandleLeft_mc.addEventListener(MouseEvent.MOUSE_OUT, endDragOutLeft, false, 0, true);

			seekHandleRight_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDownRight, false, 0, true);
			seekHandleRight_mc.addEventListener(MouseEvent.MOUSE_UP, endDragUpRight, false, 0, true);
			seekHandleRight_mc.addEventListener(MouseEvent.MOUSE_OUT, endDragOutRight, false, 0, true);
			
			seekHandleLoop_mc.addEventListener(MouseEvent.MOUSE_DOWN, dragHandleDownLoop, false, 0, true);
			seekHandleLoop_mc.addEventListener(MouseEvent.MOUSE_UP, endDragUpLoop, false, 0, true);
			seekHandleLoop_mc.addEventListener(MouseEvent.MOUSE_OUT, endDragOutLoop, false, 0, true);
		}
		
		private function dragHandleDownLoop(event:MouseEvent):void {
			seekHandleLoop_mc.addEventListener(MouseEvent.MOUSE_MOVE, moveLoopHandle);
			seekingLoop = true;
			bounds = new Rectangle(seekHandleLeft_mc.x, 5, seekHandleRight_mc.x - seekHandleLeft_mc.x, 0);
			seekHandleLoop_mc.startDrag(true, bounds);//

		}
		private function endDragUpLoop(event:MouseEvent):void {
			seekHandleLoop_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveLoopHandle);
			moveLoopComplete();
		}		
		private function endDragOutLoop(event:MouseEvent):void {
			if (seekHandleLoop_mc.hasEventListener(MouseEvent.MOUSE_MOVE))
			{
				seekHandleLoop_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveLoopHandle);
				moveLoopComplete();
			}
		}
		
		private function moveLoopHandle(event:MouseEvent):void {
			//Don`t remove this function!
		}
		
		private function moveLoopComplete():void {
			seekingLoop = false;
			seekHandleLoop_mc.stopDrag();
			fillBarLoop_mc.x = seekHandleLoop_mc.x - widthChannel;
			
			var LeftR = root['LeftRange'];
			var RightR = root['RightRange'];
			var widthBar = RightR-LeftR;
			var procHandle = (seekHandleLoop_mc.x-59)/widthChannel * 100;

			var ScopeHandle = (widthBar/100*procHandle)*100+LeftR*100;
			root['HandleOp'] = ScopeHandle;
			seekHitLoop_mc.dispatchEvent(new Event("seek_change_loop"));
		}
		
		public function moveSeekLoopPos(perc:Number):void {
			TweenMax.to(fillBarLoop_mc, 0, {x:(widthChannel * perc) + 59 - widthChannel, ease:Sine.easeIn});
			if(!seekingLoop){
				TweenMax.to(seekHandleLoop_mc, 0, {x:widthChannel * perc + 59, ease:Sine.easeIn});
				}
		}
		
		// -------- LEFT SLIDER HANDLES ------------------------------------
		private function dragHandleDownLeft(event:MouseEvent):void {
			seekHandleLeft_mc.addEventListener(MouseEvent.MOUSE_MOVE, moveLHandle);
			seeking = true;
			var SpanRight = seekHandleRight_mc.x - 59;
			bounds = new Rectangle(59, 5, SpanRight-10, 0);
			seekHandleLeft_mc.startDrag(true, bounds);
		}
		
		private function endDragUpLeft(event:MouseEvent):void {
			seekHandleLeft_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveLHandle);
			moveLComplete();
		}
		
		private function endDragOutLeft(event:MouseEvent):void {
			if (seekHandleLeft_mc.hasEventListener(MouseEvent.MOUSE_MOVE))
			{
				seekHandleLeft_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveLHandle);
				moveLComplete();
			}
		}
		
		private function moveLHandle(event:MouseEvent):void {
			fillBarL_mc.x = seekHandleLeft_mc.x - fillBarL_mc.width;
			timeLeft.x = seekHandleLeft_mc.x-43;
		}
		
		private function moveLComplete():void {
				seeking = false;
				seekHandleLeft_mc.stopDrag();
				
				var LeftR = root['LeftRange'];
				var RightR = root['RightRange'];
				var widthBar = RightR-LeftR;
				var OtLeft = seekHandleLeft_mc.x-59;
				var procLeft = OtLeft/widthChannel;
				var ScopeLeft = (widthBar*procLeft + LeftR)*100;
				fillBarL_mc.x = seekHandleLeft_mc.x - fillBarL_mc.width;
				timeLeft.x = seekHandleLeft_mc.x-43;
				root['LeftOp'] = ScopeLeft;
				//timeLeft.txt.text = formatTimeLoop(ScopeLeft);
				seekHandleLeft_mc.dispatchEvent(new Event("seekLeft_change"));
		}

		// -------- RIGHT SLIDER HANDLES ------------------------------------
		private function dragHandleDownRight(event:MouseEvent):void {
			seekHandleRight_mc.addEventListener(MouseEvent.MOUSE_MOVE, moveRHandle);
			seeking = true;
			var SpanLeft = widthChannel-seekHandleLeft_mc.x + 59;
			bounds = new Rectangle(seekHandleLeft_mc.x+10, 5, SpanLeft-10, 0);
			seekHandleRight_mc.startDrag(true, bounds);
		}
		
		private function endDragUpRight(event:MouseEvent):void {
			seekHandleRight_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveRHandle);
			moveRComplete();
		}
		
		private function endDragOutRight(event:MouseEvent):void {
			if (seekHandleRight_mc.hasEventListener(MouseEvent.MOUSE_MOVE))
			{
				seekHandleRight_mc.removeEventListener(MouseEvent.MOUSE_MOVE, moveRHandle);
				moveRComplete();
			}
		}
		
		private function moveRHandle(event:MouseEvent):void {
			fillBarR_mc.x = seekHandleRight_mc.x;
			timeRight.x = seekHandleRight_mc.x + 5;
		}
		
		private function moveRComplete():void {
			seeking = false;
			seekHandleRight_mc.stopDrag();
			
			var LeftR = root['LeftRange'];
			var RightR = root['RightRange'];
			var widthBar = RightR-LeftR;
			var OtRight = seekHandleRight_mc.x-59;
			var procRight = OtRight/widthChannel * 100;
			var ScopeRight = (LeftR*100)+((widthBar/100*procRight)*100);
			var ScopeToRight = (widthBar/100*(100-procRight))*100;
			fillBarR_mc.x = seekHandleRight_mc.x;
			timeRight.x = seekHandleRight_mc.x + 5;
			root['RightOp'] = ScopeToRight;
			root['RightOd'] = ScopeRight;
			timeRight.txt.text = formatTimeLoop(ScopeRight);
			seekHandleRight_mc.dispatchEvent(new Event("seekRight_change"));
		}
		
		public function formatTimeLoop(time:Number):String {
			//Create variables for function
			var _hours:String;
			var _minutes:String;
			var _seconds:String;
			var _delseconds:String;
			var _temp:Number;
			
			//There are 3,600,000 milliseconds in one hour
			_temp = Math.floor(time/3600000);
			
			if(_temp < 10) {
			_hours = "0" + String(_temp);
			} else {
			_hours = String(_temp);
			}
			
			//There are 60, 000 milliseconds in one minute. Use % to make sure minutes are never greater than 59
			_temp = Math.floor(time/60000) % 60;
			
			if(_temp < 10) {
			_minutes ="0" + String(_temp);
			} else {
			_minutes = String(_temp);
			}
			
			//There are 1000 milliseconds in a second. Use % to make sure that seconds are never greater than 59
			_temp = Math.floor(time/1000) % 60;
			
			if(_temp < 10) {
			_seconds = "0" + String(_temp);
			} else {
			_seconds = String(_temp);
			}
			
			_temp = Math.floor(time/10) % 100;
			
			if(_temp < 10) {
			_delseconds = "0" + String(_temp);
			} else {
			_delseconds = String(_temp);
			}
			
			return _minutes + ":" +  _seconds + "." +  _delseconds;
		}
	}
}