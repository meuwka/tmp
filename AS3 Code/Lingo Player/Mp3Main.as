﻿package {
	import flash.events.*;
	import fl.events.ListEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
    import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.ProgressEvent;
	import flash.utils.Timer;
	import flash.external.ExternalInterface;
	//&
	import flash.text.TextField;  
    import flash.text.TextFormat;  
    import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	import flash.text.AntiAliasType;
	import flash.filters.DropShadowFilter;
	
	import com.greensock.*;
	
	public class Mp3Main extends MovieClip{
		public var song:Sound;
		public var channel:SoundChannel;
		public var sndTrans:SoundTransform;
		public var position:Number;
		public var positionLoop:Number;
		public var mp3_path:String;
		public var LeftRange:Number = 0;
		public var RightRange:Number = 0;
		
		public var LeftOp:Number = 0;
		public var HandleOp:Number = 0;
		public var RightOp:Number = 0;
		public var RightOd:Number = 0;
		
		private var updateTime:Timer;
		public var arMarker:Array;
		private var num:int;
		private var flagReplay:Boolean;
		private var startReplPos:Number;
		private var endReplPos:Number;
		private var flagStop:Boolean;
		protected var _defaultHintFormat:TextFormat = new TextFormat("Arial", 10, 0xFFFFFF);
		protected var displayFormatOrange:TextFormat = new TextFormat(null, null, 0xBC5400);
		protected var displayFormatSmall:TextFormat = new TextFormat(null, 12, 0x000000);
		private var positionTimer:Timer;
		
		private var hint_mc:Sprite;

		public function Mp3Main() {
			position = 0;

			mode_mc.alpha = 0;
			removeChild(mode_mc);
			
			sndTrans = new SoundTransform(vol_mc.percent);
			loadMP3();
			
			if (ExternalInterface.available){
                try {
                    ExternalInterface.addCallback("sendArrayFromJS", receivedArrayFromJS);
					if (!checkJavaScriptReady()) {
                        var readyTimer:Timer = new Timer(100, 0);
                        readyTimer.addEventListener(TimerEvent.TIMER, timerHandler);
                        readyTimer.start();
                    }
                } catch (error:Error) {
                    trace("An Error occurred: " + error.message);
                }
            } else {
                trace("External interface is not available for this container.");
            }
		}
		
		private function receivedArrayFromJS(arrValue:Array):void {
			arMarker = null;
			arMarker = arrValue;
			num = arMarker.length;
        }
		
		private function checkJavaScriptReady():Boolean {
			var isReady:Boolean = ExternalInterface.call("isReady");
			return isReady;
        }
			
		private function timerHandler(event:TimerEvent):void {
			var isReady:Boolean = checkJavaScriptReady();
			//display_mc.timeDisp.text = "JavaScript says: no ready";
			if (isReady) {
				Timer(event.target).stop();
				//display_mc.timeDisp.text = "now ready";
			}
		}
		
		/*This function is used to display the hours, minutes and seconds.
		It is only used in the updateSeek() function.*/
		public function formatTime(time:Number):String {
			//Create variables for function
			var _hours:String;
			var _minutes:String;
			var _seconds:String;
			var _temp:Number;
			
			//There are 3,600,000 milliseconds in one hour
			_temp = Math.floor(time/3600000);
			
			if(_temp < 10) {
			_hours = "0" + String(_temp);
			} else {
			_hours = String(_temp);
			}
			
			//There are 60, 000 milliseconds in one minute. Use % to make sure minutes are never greater than 59
			_temp = Math.floor(time/60000) % 60;
			
			if(_temp < 10) {
			_minutes ="0" + String(_temp);
			} else {
			_minutes = String(_temp);
			}
			
			//There are 1000 milliseconds in a second. Use % to make sure that seconds are never greater than 59
			_temp = Math.floor(time/1000) % 60;
			
			if(_temp < 10) {
			_seconds = "0" + String(_temp);
			} else {
			_seconds = String(_temp);
			}
			
			return _hours + ":" + _minutes + ":" +  _seconds;
		}
		
	/***********************Event Handlers****************************/
		
		private function loadMP3():void
		{
			if (stage.loaderInfo.parameters['mp3Url']!=undefined) {
			mp3_path=String(stage.loaderInfo.parameters['mp3Url']);
			}
			else{mp3_path="Alice_in_Wonderland.mp3";}
			// Create new Sound object
			song = new Sound();
			song.addEventListener(ProgressEvent.PROGRESS, mp3FileLoading);
			song.addEventListener(Event.COMPLETE, mp3FileLoaded);
			song.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			// Our mp3 that we want to load
			song.load(new URLRequest(mp3_path));
			display_mc.timeDisp.text ="LOADING...";
			
		}
		
		private function mp3FileLoading(e:ProgressEvent):void
		{
			var currentPercent:Number = Math.round(e.bytesLoaded / e.bytesTotal * 100);
			display_mc.timeDisp.text = 'LOADING...' + currentPercent + '%';
			
			// Test if song have loaded at least 100 percent
			if (currentPercent == 100)
			{
				//code for play here!
				song.removeEventListener(ProgressEvent.PROGRESS, mp3FileLoading);
				TweenMax.to(locked_mc, 0.5, {alpha:0, onComplete: function(){removeChild(locked_mc);}});
			}
		}
		
		private function mp3FileLoaded(e:Event):void
		{
			song.removeEventListener(Event.COMPLETE, mp3FileLoaded);
			
			vol_mc.addEventListener("volume_change", updateVolume);
			vol_mc.buttonMode = true;
			seek_mc.addEventListener("seek_change", changeSeekBar);
			seek_mc.addEventListener("seek_up", UpSeekBar);
			seek_mc.addEventListener("seek_down", DownSeekBar);
			seek_mc.buttonMode = true;
			
			mode_mc.seek2_mc.seekHandleLeft_mc.addEventListener("seekLeft_change", changeSeekLeftBar);
			mode_mc.seek2_mc.seekHandleRight_mc.addEventListener("seekRight_change", changeSeekRightBar);
			mode_mc.seek2_mc.seekHitLoop_mc.addEventListener("seek_change_loop", changeSeekLoopBar);
			mode_mc.seek2_mc.seekHandleLeft_mc.buttonMode = true;
			mode_mc.seek2_mc.seekHandleLoop_mc.buttonMode = true;
			mode_mc.seek2_mc.seekHandleRight_mc.buttonMode = true;
			mode_mc.seek2_mc.seekHitLoop_mc.buttonMode = true;

			//mode_mc.seek2_mc.fillBarLoop_mc.buttonMode = true;

			
			//mode_mc.seek2_mc.addEventListener("seek2_change", changeSeekBar);
			//mode_mc.seek2_mc.addEventListener("seek2_up", UpSeekBar);
			//mode_mc.seek2_mc.addEventListener("seek2_down", DownSeekBar);
			//mode_mc.seek2_mc.buttonMode = true;
			
		/*****************Button Event Listeners************************/
			playPause_mc.addEventListener(MouseEvent.MOUSE_OVER, PlayOver);
			playPause_mc.addEventListener(MouseEvent.MOUSE_OUT, PlayOut);
			playPause_mc.addEventListener(MouseEvent.MOUSE_DOWN, PlayDown);
			playPause_mc.addEventListener(MouseEvent.MOUSE_UP, PlayUp);
			playPause_mc.buttonMode = true;
			
			mute_mc.addEventListener(MouseEvent.MOUSE_OVER, MuteOver);
			mute_mc.addEventListener(MouseEvent.MOUSE_OUT, MuteOut);
			mute_mc.addEventListener(MouseEvent.MOUSE_DOWN, MuteDown);
			mute_mc.addEventListener(MouseEvent.MOUSE_UP, MuteUp);
			mute_mc.buttonMode = true;
			
		    back_mc.addEventListener(MouseEvent.MOUSE_OVER, BackOver);
			back_mc.addEventListener(MouseEvent.MOUSE_OUT, BackOut);
			back_mc.addEventListener(MouseEvent.MOUSE_DOWN, BackDown);
			back_mc.addEventListener(MouseEvent.MOUSE_UP, BackUp);
			back_mc.buttonMode = true;
			
			forward_mc.addEventListener(MouseEvent.MOUSE_OVER, ForwardOver);
			forward_mc.addEventListener(MouseEvent.MOUSE_OUT, ForwardOut);
			forward_mc.addEventListener(MouseEvent.MOUSE_DOWN, ForwardDown);
			forward_mc.addEventListener(MouseEvent.MOUSE_UP, ForwardUp);
			forward_mc.buttonMode = true;
			
			replay_mc.addEventListener(MouseEvent.MOUSE_OVER, ReplayOver);
			replay_mc.addEventListener(MouseEvent.MOUSE_OUT, ReplayOut);
			replay_mc.addEventListener(MouseEvent.MOUSE_DOWN, ReplayDown);
			replay_mc.addEventListener(MouseEvent.MOUSE_UP, ReplayUp);
			replay_mc.buttonMode = true;
			flagReplay = false;
			flagStop = false;
			
			stop_mc.addEventListener(MouseEvent.MOUSE_OVER, StopOver);
			stop_mc.addEventListener(MouseEvent.MOUSE_OUT, StopOut);
			stop_mc.addEventListener(MouseEvent.MOUSE_DOWN, StopDown);
			stop_mc.addEventListener(MouseEvent.MOUSE_UP, StopUp);
			stop_mc.buttonMode = true;
			
			this.addEventListener(Event.ENTER_FRAME, updateSeek);
		}
		
		private function soundComplete(event:Event):void {
			channel.removeEventListener(Event.SOUND_COMPLETE, soundComplete);
			returnInStart();
		}
		
		private function returnInStart():void {
			if(playPause_mc.playing){
				channel.stop();
				playPause_mc.playing = false;
				position = 0;
				seek_mc.moveSeekPos(0);
				playPause_mc.gotoAndStop('play');
				updateTime.stop();
				updateTime.removeEventListener(TimerEvent.TIMER, getSeek)
			}
			else{
				position = 0;
				seek_mc.moveSeekPos(0);
			}
			ExternalInterface.call("dragHandle", Math.floor(position/100));
		}
		
		/*The next two are called handling custom events from within SeekControl.as
		and VolControl.as*/
		private function updateVolume(event:Event):void {
			sndTrans.volume = vol_mc.percent;
			if(channel != null){			
			channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
			}
		}
		private function changeSeekBar(event:Event):void {
			//isFlagReplay();
			//isFlagStop();
			position = (song.length/(song.bytesLoaded/song.bytesTotal)) * seek_mc.percent;
			if(playPause_mc.playing){
				channel.stop();
				channel = song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
				channel.addEventListener(Event.SOUND_COMPLETE, soundComplete);
			}
		}
		
		private function changeSeekLeftBar(event:Event):void {
			replay_mc.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP, true, false));
		}
		
		private function changeSeekRightBar(event:Event):void {
			if(!playPause_mc.playing || (mode_mc.seek2_mc.seekHandleRight_mc.x <= mode_mc.seek2_mc.seekHandleLoop_mc.x)){
				replay_mc.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP, true, false));
			}
		}
		
		private function changeSeekLoopBar(event:Event):void {
			position = HandleOp;
			if(playPause_mc.playing){
				channel.stop();
				channel = song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
				channel.addEventListener(Event.SOUND_COMPLETE, soundComplete);
			}
			/*var widthBar = RightRange-LeftRange;
			var percloop:Number = (Math.floor(channel.position/100)- LeftRange) / widthBar;
		    mode_mc.seek2_mc.moveSeekLoopPos(percloop);*/
		}
		
		private function UpSeekBar(event:Event):void {
			if(channel != null){
				if(updateTime.running){
					updateTime.stop();
					updateTime.removeEventListener(TimerEvent.TIMER, getSeek);
				}
				if(playPause_mc.playing) {
				updateTime = new Timer(100);
				updateTime.start();
				updateTime.addEventListener(TimerEvent.TIMER, getSeek);
				}
			}
			ExternalInterface.call("dragHandle", Math.floor(position/100));
		}
		private function DownSeekBar(event:Event):void {
			if(channel != null){
				if(updateTime.running){
					updateTime.stop();
					updateTime.removeEventListener(TimerEvent.TIMER, getSeek);
				}
			}
			ExternalInterface.call("dragHandle", -1);
		}
		
		//update time
		private function updateSeek(event:Event):void {
			if(channel != null && playPause_mc.playing) {
				var perc:Number = channel.position / (song.length/(song.bytesLoaded/song.bytesTotal));
				seek_mc.moveSeekPos(perc);
				display_mc.timeDisp.text = formatTime(channel.position) + " of " + formatTime((song.length/(song.bytesLoaded/song.bytesTotal)));
			}else {
				display_mc.timeDisp.text = formatTime(position) + " of " + formatTime((song.length/(song.bytesLoaded/song.bytesTotal)));
			}
			display_mc.timeDisp.setTextFormat(displayFormatOrange, 0, 8);
			display_mc.timeDisp.setTextFormat(displayFormatSmall, 9, 11);
		}
		
		private function getSeek(event:Event):void {
			if(channel != null && playPause_mc.playing) {
				ExternalInterface.call("sendVariable", Math.floor(channel.position/100), Math.floor((song.length/(song.bytesLoaded/song.bytesTotal))/100));
			}
		}
		
		private function ioErrorHandler(event:Event):void {
			trace("Error loading sound: " + event);
		}
		
		private function createHint(textHint:String, xx:int, yy:int):void{
			hint_mc = new Sprite();
            var t:TextField = new TextField();
            t.autoSize = TextFieldAutoSize.LEFT;
            t.wordWrap = false;
			t.selectable = false;
            t.defaultTextFormat = this._defaultHintFormat;
			t.text = textHint;
			hint_mc.mouseEnabled = false;
			hint_mc.mouseChildren = false;
			hint_mc.graphics.lineStyle(1, 0xFFFFFF);
            hint_mc.graphics.beginFill(0x000000);
			hint_mc.graphics.moveTo(0, 0);
            hint_mc.graphics.lineTo(t.width+6, 0);
            hint_mc.graphics.lineTo(t.width+6, t.height);
            hint_mc.graphics.lineTo(t.width * 0.5 + 3, t.height);
            hint_mc.graphics.lineTo(t.width * 0.5, t.height + 5);
            hint_mc.graphics.lineTo(t.width * 0.5 - 3, t.height);
            hint_mc.graphics.lineTo(0, t.height);
            hint_mc.graphics.lineTo(0, 0);
            hint_mc.graphics.endFill();
			
			hint_mc.x = xx + 5 - t.width * 0.5;
			hint_mc.y = yy - 20;
			//TweenMax.to(hint_mc, 0, {dropShadowFilter:{color:0x000000, alpha:0.5, angle:45, blurX:2, blurY:2, distance:2}});
			hint_mc.filters = [new DropShadowFilter(2, 45, 0, 0.5)];
			hint_mc.alpha = 0.55;
			hint_mc.addChild(t);
			t.x +=3;
			addChild(hint_mc);
		}
		
	/*******************PlayPause Handlers*************************/
		private function PlayOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			if(obj.playing) {
				obj.gotoAndStop('pause_over');
				createHint("Pause", obj.x, obj.y);
			} else {
				obj.gotoAndStop('play_over');
				createHint("Play", obj.x, obj.y);
			}
		}
		private function PlayOut(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			removeChild(hint_mc);
			if(obj.playing) {
				obj.gotoAndStop('pause');
			} else {
				obj.gotoAndStop('play');
			}
		}
		private function PlayDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			if(obj.playing) {
				obj.gotoAndStop('pause_down');
			} else {
				obj.gotoAndStop('play_down');
			}
			ResetBarMode2();
		}
		/*Needs to switch between a pause and play button.*/
		private function PlayUp(event:MouseEvent):void {
			isFlagReplay();
			isFlagStop();
			var obj:Object = event.currentTarget;
			removeChild(hint_mc);
			if(obj.playing) {
				//this.removeEventListener(Event.ENTER_FRAME, updateSeek);
				ExternalInterface.call("pauseScroll");
				obj.gotoAndStop('play_over');
				createHint("Play", obj.x, obj.y);
				position = channel.position;
				channel.stop();
				obj.playing = false;
				updateTime.stop();
				updateTime.removeEventListener(TimerEvent.TIMER, getSeek);
			} else {
					obj.gotoAndStop('pause_over');
					createHint("Pause", obj.x, obj.y);
					channel = song.play(position);
					channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
					obj.playing = true;
					updateTime = new Timer(100);
					updateTime.start();
					updateTime.addEventListener(TimerEvent.TIMER, getSeek);
					ExternalInterface.call("sendVariable", Math.floor(channel.position/100), Math.floor((song.length/(song.bytesLoaded/song.bytesTotal))/100));
					channel.addEventListener(Event.SOUND_COMPLETE, soundComplete);
			}
		}
		
	/********************Mute Handlers************************/
		private function MuteOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			if(obj.isMute) {
				obj.gotoAndStop('unmute_over');
				createHint("Unmute", obj.x, obj.y);
			} else {
				obj.gotoAndStop('mute_over');
				createHint("Mute", obj.x, obj.y);
			}
		}
		private function MuteOut(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			removeChild(hint_mc);
			if(obj.isMute) {
				obj.gotoAndStop('unmute');
			} else {
				obj.gotoAndStop('mute');
			}
		}
		private function MuteDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			if(obj.isMute) {
				obj.gotoAndStop('unmute_down');
			} else {
				obj.gotoAndStop('mute_down');
			}
		}
		private function MuteUp(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			removeChild(hint_mc);
			if(obj.isMute) {
				obj.gotoAndStop('mute_over');
				createHint("Mute", obj.x, obj.y);
				obj.isMute = false;
				if(channel != null){channel.soundTransform = sndTrans;}
			} else {
				obj.gotoAndStop('unmute_over');
				createHint("Unmute", obj.x, obj.y);
				obj.isMute = true;
				if(channel != null){channel.soundTransform = new SoundTransform(0);}
			}
		}
		
		/********************Back Handlers************************/
		private function BackOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('back_over');
			createHint("Previous", obj.x, obj.y);
		}
		private function BackOut(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('back');
			removeChild(hint_mc);
		}
		private function BackDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('back_down');
		}
		/*changes the List selection and then dispatches a fake ITEM_CLICK event.*/
		private function BackUp(event:MouseEvent):void {
			ResetBarMode2();
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('back_over');
			isFlagReplay();
			isFlagStop();
			var rsec = 0;		
			if(playPause_mc.playing){rsec = Math.floor(channel.position/100);}
			else {rsec = Math.floor(position/100);}
			
			for (var s = 0; s < num; s++){
				if (rsec >= arMarker[s][0] && rsec < arMarker[s+1][0]){
					//display_mc.timeDisp.text = " pos " + arMarker[s][0]+ " newpos: " + arMarker[s-1][0];
					if (arMarker[s][0] == 0){
						RewindPlay(arMarker[0][0], arMarker[1][0]);
					}
					else {
						RewindPlay(arMarker[s-1][0], arMarker[s][0]);
					}
					break;
				}
			}
		}

	/********************Forward Handlers************************/
	
		private function ForwardOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('forward_over');
			createHint("Next", obj.x, obj.y);
		}
		private function ForwardOut(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('forward');
			removeChild(hint_mc);
		}
		private function ForwardDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('forward_down');
		}

		private function ForwardUp(event:MouseEvent):void {
			ResetBarMode2();
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('forward_over');
			isFlagReplay();
			isFlagStop();
			var rsec = 0;
			if(playPause_mc.playing){rsec = Math.floor(channel.position/100);}
			else {rsec = Math.floor(position/100);}
			for (var s = 0; s < num; s++){
				if (rsec >= arMarker[s][0] && rsec < arMarker[s+1][0]){
					//display_mc.timeDisp.text = "sec" + rsec + "newpos: " + arMarker[s+1][0];
					if ( arMarker[s+1][0] == arMarker[num-1][0]){RewindStop();}
					else {RewindPlay(arMarker[s+1][0], arMarker[s+2][0]);}
					break;
				}
			}
		}
		
		private function RewindPlay(newposition:Number, newendposition:Number):void {
			ExternalInterface.call("dragHandle", newposition);
			newposition*=100;
			position =newposition;
			var perc:Number = newposition / (song.length/(song.bytesLoaded/song.bytesTotal));
			seek_mc.moveSeekPos(perc);
			if(playPause_mc.playing){
				channel.stop();
				channel=song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
			}
		}
		
		private function RewindStop():void {
			returnInStart();
		}
		
		/********************Replay************************/
		
		private function ReplayOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('replay_over');
			createHint("Replay", obj.x, obj.y);
		}
		
		private function ReplayOut(event:MouseEvent):void {
			if(!flagReplay){
				var obj:Object = event.currentTarget;
				obj.gotoAndStop('replay');
			} 
			removeChild(hint_mc);
		}
		
		private function ReplayDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('replay_down');
		}
		
		private function ReplayUp(event:MouseEvent):void {
			addChildAt(mode_mc, numChildren - 2);
			TweenMax.to(mode_mc, 0.5, {alpha:1});
		
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('replay_over');
			isFlagStop();
			flagReplay = true;
			var rsec = 0;
			if(playPause_mc.playing){
				rsec = Math.floor(channel.position/100);
			}
			else {
				rsec = Math.floor(position/100);
			}
			
			for (var s = 0; s < num; s++){
				if (rsec >= arMarker[s][0] && rsec < arMarker[s+1][0]){
					startReplPos = arMarker[s][0];
					endReplPos = arMarker[s+1][0];
					PlayPart(startReplPos, endReplPos);
					LeftRange = startReplPos;
					RightRange = endReplPos;
					break;
				}
			}
		}
		
		private function isFlagReplay():void {
			if(flagReplay){
				flagReplay = false;
			}
				RightOp = 0;
				RightOd = 0;
				LeftOp = 0;
				mode_mc.seek2_mc.seekHandleLoop_mc.x = 59;
				mode_mc.seek2_mc.fillBarLoop_mc.x = mode_mc.seek2_mc.seekHandleLoop_mc.x - mode_mc.seek2_mc.widthChannel;
				mode_mc.seek2_mc.seekHandleLeft_mc.x = 59;
				mode_mc.seek2_mc.seekHandleRight_mc.x = 643;
				mode_mc.seek2_mc.fillBarL_mc.x = -526;
				mode_mc.seek2_mc.fillBarR_mc.x = 642;
				mode_mc.seek2_mc.timeLeft.x = 16;
				mode_mc.seek2_mc.timeRight.x = 648;
		}

		public function PlayPart(startpos:Number, endpos:Number):void {
			startpos*=100;
			endpos*=100;
			if (LeftOp != 0)
			{
				startpos = LeftOp;
			}
			if (RightOd != 0)
			{
				endpos = RightOd;
			}
			ResetModeRep2(startpos, endpos);
			position =startpos;
			var perc:Number = startpos / (song.length/(song.bytesLoaded/song.bytesTotal));
			seek_mc.moveSeekPos(perc);
			if(playPause_mc.playing){
				channel.stop();
				channel=song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
			}
			else{
				playPause_mc.gotoAndStop('pause');
				channel = song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
				playPause_mc.playing = true;
				ExternalInterface.call("dragHandle", startReplPos);
			}
			positionTimer = new Timer(10);
            positionTimer.addEventListener(TimerEvent.TIMER, positionTimerHandler);
            positionTimer.start();
			//this.addEventListener(Event.ENTER_FRAME, isEndReplPos);
		}
		
		/*private function isEndReplPos(event:Event):void {
			//var widthBar = RightRange-LeftRange;
			var perem = channel.position/100;
			if (flagReplay){
				//var percloop:Number = (perem - LeftRange) / widthBar;
				//percloop = Math.floor(percloop *100)/100;
		   		//mode_mc.seek2_mc.moveSeekLoopPos(percloop);
				//mode_mc.seek2_mc.timeLeft.txt.text = percloop.toString();
				if (RightOp != 0)
				{
					perem += RightOp/100;
				}
				perem = Math.floor(perem);
				
				//display_mc.timeDisp.text = perem.toString()+ " / "+endReplPos.toString();
				if (perem > endReplPos - 1) {
					this.removeEventListener(Event.ENTER_FRAME, isEndReplPos);
					replay_mc.gotoAndStop('replay');
					flagReplay = false;
					startReplPos = -1;
					endReplPos = -1;
					playPause_mc.gotoAndStop('play');
					position = channel.position - 100;
					channel.stop();
					playPause_mc.playing = false;
				}
			}
			else{
				this.removeEventListener(Event.ENTER_FRAME, isEndReplPos);
				replay_mc.gotoAndStop('replay');
			}
		}*/
		
		private function positionTimerHandler(event:TimerEvent):void {
			var widthBar = RightRange-LeftRange;
			var perem = channel.position;
			if (flagReplay){
				var percloop:Number = ( perem/100 - LeftRange) / widthBar;
				percloop = Math.floor(percloop *100)/100;
		   		mode_mc.seek2_mc.moveSeekLoopPos(percloop);
				//mode_mc.seek2_mc.timeLeft.txt.text = percloop.toString();
				if (RightOp != 0)
				{
					perem += RightOp;
				}
				perem = Math.floor(perem/100);
				
				//display_mc.timeDisp.text = perem.toString()+ " / "+endReplPos.toString();
				if (perem > endReplPos - 1) {
					positionTimer.stop();
					positionTimer.removeEventListener(TimerEvent.TIMER, positionTimerHandler);
					//this.removeEventListener(Event.ENTER_FRAME, isEndReplPos);
					replay_mc.gotoAndStop('replay');
					flagReplay = false;
					startReplPos = -1;
					endReplPos = -1;
					playPause_mc.gotoAndStop('play');
					position = channel.position - 100;
					channel.stop();
					playPause_mc.playing = false;
				}
			}
			else{
				positionTimer.stop();
				positionTimer.removeEventListener(TimerEvent.TIMER, positionTimerHandler);
				//this.removeEventListener(Event.ENTER_FRAME, isEndReplPos);
				replay_mc.gotoAndStop('replay');
			}
		}
		
		/********************Stop Handlers************************/
		private function StopOver(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('stop_over');
			createHint("Play Next", obj.x, obj.y);
		}
		private function StopOut(event:MouseEvent):void {
			if (!flagStop){
				var obj:Object = event.currentTarget;
				obj.gotoAndStop('stop');
			}
			removeChild(hint_mc);
		}
		private function StopDown(event:MouseEvent):void {
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('stop_down');
		}

		private function StopUp(event:MouseEvent):void {
			addChildAt(mode_mc, numChildren - 2);
			TweenMax.to(mode_mc, 0.5, {alpha:1});
			
			var obj:Object = event.currentTarget;
			obj.gotoAndStop('stop_over');
			flagReplay = false;
			flagStop = true;
			var rsec = 0;
			if(playPause_mc.playing){
				rsec = Math.floor(channel.position/100);
			}
			else {
				rsec = Math.floor(position/100);
			}
			for (var s = 0; s < num; s++){
				if (rsec >= arMarker[s][0] && rsec < arMarker[s+1][0]){
					endReplPos = arMarker[s+2][0];
					startReplPos = arMarker[s+1][0];
					StopPart(startReplPos, endReplPos);
					ResetMode2(startReplPos,endReplPos);
					break;
				}
			}
		}
		
		private function isFlagStop():void {
			if(flagStop){
				flagStop = false;
			}
		}
		
		private function StopPart(startpos:Number, endpos:Number):void {
			this.addEventListener(Event.ENTER_FRAME, isStopPos);
			startpos*=100;
			var perc:Number = startpos / (song.length/(song.bytesLoaded/song.bytesTotal));
			this.addEventListener(Event.ENTER_FRAME, isStopPos);
			position =startpos;
			seek_mc.moveSeekPos(perc);
			if(playPause_mc.playing){
				channel.stop();
				channel=song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
			}
			else{
				playPause_mc.gotoAndStop('pause');
				channel = song.play(position);
				channel.soundTransform = (mute_mc.isMute)? new SoundTransform(0) : sndTrans;
				playPause_mc.playing = true;
			}
			ExternalInterface.call("dragHandle", startReplPos);
		}
		
		private function isStopPos(event:Event):void {
			var widthBar = RightRange-LeftRange;
			var percloop:Number = (channel.position/100- LeftRange) / widthBar;
		    mode_mc.seek2_mc.moveSeekLoopPos(percloop);
			
			if(flagStop){
				var perem = Math.floor(channel.position/100);
				if (RightOp != 0)
				{
					perem = perem+RightOp/100;
				}
				if(perem > endReplPos-1) {
					this.removeEventListener(Event.ENTER_FRAME, isStopPos);
					stop_mc.gotoAndStop('stop');
					flagStop = false;
					//ExternalInterface.call("dragHandle", endReplPos);
					startReplPos = -1;
					endReplPos = -1;
					playPause_mc.gotoAndStop('play');
					position = channel.position - 100;
					channel.stop();
					playPause_mc.playing = false;
				}
			}
			else{
				this.removeEventListener(Event.ENTER_FRAME, isStopPos);
				stop_mc.gotoAndStop('stop');
			}
		}
		
		private function ResetMode2(startP,endP)
		{
			RightOp = 0;
			RightOd = 0;
			LeftOp = 0;
			LeftRange = startP;
			RightRange = endP;
			mode_mc.seek2_mc.seekHandleLoop_mc.x = 59;
			mode_mc.seek2_mc.fillBarLoop_mc.x = mode_mc.seek2_mc.seekHandleLoop_mc.x - mode_mc.seek2_mc.widthChannel;
			mode_mc.seek2_mc.seekHandleLeft_mc.x = 59;
			mode_mc.seek2_mc.seekHandleRight_mc.x = 643;
			mode_mc.seek2_mc.fillBarL_mc.x = -526;
			mode_mc.seek2_mc.fillBarR_mc.x = 643;
			mode_mc.seek2_mc.timeLeft.x = 16;
			mode_mc.seek2_mc.timeRight.x = 648;
			mode_mc.seek2_mc.timeLeft.txt.text = mode_mc.seek2_mc.formatTimeLoop(startP*100);
			mode_mc.seek2_mc.timeRight.txt.text = mode_mc.seek2_mc.formatTimeLoop(endP*100);
		}
		private function ResetModeRep2(startP,endP)
		{
			
			//mode_mc.seek2_mc.seekHandleLoop_mc.x = mode_mc.seek2_mc.seekHandleLeft_mc.x;
			//mode_mc.seek2_mc.fillBarLoop_mc.x = mode_mc.seek2_mc.seekHandleLoop_mc.x - mode_mc.seek2_mc.fillBarLoop_mc.width;
			mode_mc.seek2_mc.timeLeft.txt.text = mode_mc.seek2_mc.formatTimeLoop(startP);
			mode_mc.seek2_mc.timeRight.txt.text = mode_mc.seek2_mc.formatTimeLoop(endP);
		}
		private function ResetBarMode2()
		{
			TweenMax.to(mode_mc, 0.5, {alpha:0, onComplete: function(){removeChild(mode_mc);}});
		}
		
//end
	}
}