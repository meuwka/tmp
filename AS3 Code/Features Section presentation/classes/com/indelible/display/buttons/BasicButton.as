﻿package com.indelible.display.buttons {

	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	import fl.transitions.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	
	public class BasicButton extends Sprite {
		
		public var offImage:DisplayObject;
		public var onImage:DisplayObject;
		public var isOnImageHalo:Boolean = false;
		private var myAction:Function;
		
		//a constructor for when the images are defined on the stage
		public function BasicButton(...List) {
			if (List.length > 1)
			{
				isOnImageHalo = true; 
				// 0 is image, 1 is rollover
				
				var l2:Loader = new Loader();
				//l2.contentLoaderInfo.addEventListener(Event.INIT,loadOffImageComplete);
				l2.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
				l2.load(new URLRequest(List[0]));								
				offImage = l2;
				addChild(l2);
				
				var l1:Loader = new Loader();
				//l1.contentLoaderInfo.addEventListener(Event.INIT,loadOnImageComplete);
				l1.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
				l1.load(new URLRequest(List[1]));				
				onImage = l1;
				addChild(l1);				
				
			}			
			
			//to catch non-explicit instentiations
			if(onImage == null && this.getChildByName("on_image") != null)
				onImage = this["on_image"];
				
			if(offImage == null && this.getChildByName("off_image") != null)
				offImage = this["off_image"];				
				
			
			if (offImage != null) offImage.visible = true;
			if (onImage != null) onImage.visible = false;
			this.buttonMode = true;
			
			this.addEventListener(MouseEvent.MOUSE_OVER, rollOverImpl);
			this.addEventListener(MouseEvent.MOUSE_OUT, rollOutImpl);
			
		}
		
		public function setAction(action:Function) {
			myAction = action;
			this.addEventListener(MouseEvent.CLICK, action);
		}
		
		public function turnOn():void
		{
			this.removeEventListener(MouseEvent.MOUSE_OVER, rollOverImpl);
			this.removeEventListener(MouseEvent.MOUSE_OUT, rollOutImpl);			
			this.removeEventListener(MouseEvent.CLICK, myAction);					
			this.buttonMode = false;
			if (!isOnImageHalo)
				offImage.visible = false;							
			onImage.visible = true;			
		}
		
		public function turnOff():void
		{
			this.addEventListener(MouseEvent.MOUSE_OVER, rollOverImpl);
			this.addEventListener(MouseEvent.MOUSE_OUT, rollOutImpl);
			this.addEventListener(MouseEvent.CLICK, myAction);			
			this.buttonMode = true;		
			if (!isOnImageHalo)
				offImage.visible = true;				
			onImage.visible = false;			
		}		
		
		protected function rollOverImpl(e:Event) {
			if (!isOnImageHalo)
				if (offImage != null) offImage.visible = false;							
			
			if (onImage != null) onImage.visible = true;
		}
		
		protected function rollOutImpl(e:Event) {
			if (!isOnImageHalo)
				if (offImage != null) offImage.visible = true;				
			
			if (onImage != null) onImage.visible = false;
		}		
		
		private function onError(e:Event):void
		{
			trace("BasicButton Error:" + e);
		}		
		
	}
}