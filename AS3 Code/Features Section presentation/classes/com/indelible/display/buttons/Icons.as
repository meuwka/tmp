﻿package com.indelible.display.buttons {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	import flash.filters.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import com.indelible.utils.*;
	
	public class Icons extends BasicButton {
		
		//from fla file
		public var imageSheet:Sprite;
		public var imageSheetOn:Sprite;
		
		public var iconWidth:int = 90;
		public var iconHeight:int = 55;
		public var border:int = 4;
		
		public var isActive:Boolean = false;
		public var featureImageOn:String;
		public var featureImageOff:String;
		public var cameraImage:String;
		
		//an object that holds the position and name of all the icons
		//for example...
		public var iconStruct:Object = {
			"icon name 1": {row:0, col:0},
			"icon name 2": {row:0, col:1}
		};
		
		public function Icons(...List) {
			//trace("Icons() " + onImage);
			
			super(List);
		}
		
		public function init(iconStruct, iconWidth, iconHeight, border=4) {
			
			this.iconStruct = iconStruct;
			this.iconWidth = iconWidth;
			this.iconHeight = iconHeight;
			this.border = border;
			
			
			function loader(pat:String, mc:Sprite){
				var image1:DisplayObject;
			
				var loader1:Loader = new Loader();
				loader1.contentLoaderInfo.addEventListener(Event.COMPLETE, done1);
				
				function done1(e:Event){
						mc.addChild(image1);
				}
				
				loader1.load(new URLRequest(pat));
				image1 = loader1;
			}
			
			//hoon 5/14 - bring in the images for the icons
			if(this.iconStruct.iconName == "feature"){
				featureImageOn = this.iconStruct.featureImageOn;
				featureImageOff = this.iconStruct.featureImageOff;

				loader(featureImageOn, imageSheetOn);
				loader(featureImageOff, imageSheet);
				
			} else if(this.iconStruct.iconName == "camera"){
				cameraImage = this.iconStruct.cameraImage;

				loader(cameraImage, imageSheet);
			}
				
			offImage = imageSheet;
			if (imageSheetOn != null)
				onImage = imageSheetOn;			
			
			if (imageSheet != null) { 
				imageSheet.visible = false;
				
				var _rectangle:Sprite = new Sprite();
				_rectangle.graphics.lineStyle(0, 0xffffff, 0);
				_rectangle.graphics.beginFill(0xffffff, 1);
				_rectangle.graphics.drawRect(0, 0, iconWidth, iconHeight);
				
				imageSheet.mask = _rectangle;
				
				addChild(_rectangle);
			}
			
			if (imageSheetOn != null) { 
				imageSheetOn.visible = false;
				
				var _rectangle:Sprite = new Sprite();
				_rectangle.graphics.lineStyle(0, 0xffffff, 0);
				_rectangle.graphics.beginFill(0xffffff, 1);
				_rectangle.graphics.drawRect(0, 0, iconWidth, iconHeight);
				
				imageSheetOn.mask = _rectangle;
				
				addChild(_rectangle);
			}			
		}
		

		public function show(iconName:String) {
			trace("Icons.show("+iconName+") " + imageSheetOn);
			
			if (iconName == null || iconName=="") {
				return;
			}
			
			if (iconStruct == null || imageSheet == null) return;
			
			if (iconStruct[iconName] == null) {
				trace("Icons.show("+iconName+") ERROR: icon not found");
				return;
			}
			
			isActive = true;
			
			var row:int = iconStruct[iconName].row;
			var col:int = iconStruct[iconName].col;
			
			trace (row, col);
			
			imageSheet.x = -col*(iconWidth+border);
			imageSheet.y = -row*(iconHeight+border);
			if (imageSheetOn != null) {
				imageSheetOn.x = -col*(iconWidth+border);
				imageSheetOn.y = -row*(iconHeight+border);			
			}
			
			//trace(imageSheet.x, imageSheet.y);
			
			imageSheet.visible = true;
			
			if (iconStruct[iconName].targetURL != null && iconStruct[iconName].targetURL != "") {
				setAction(function (e:Event) {
					var request:URLRequest = new URLRequest(iconStruct[iconName].targetURL);
					navigateToURL(request, iconStruct.target);
											});
			}
		}
		
		public function hide() {
			//trace("Icons.hide()");
			isActive = false;
			imageSheet.visible = false;
		}
		
	}
	
}