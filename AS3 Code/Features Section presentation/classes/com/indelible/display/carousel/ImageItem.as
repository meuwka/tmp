﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	//import fl.transitions.easing.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import com.indelible.utils.*;
	import com.indelible.display.buttons.*;
	
	public class ImageItem extends Sprite implements CarouselItem {
	
		public var closeButton:Sprite;
		public var image:DisplayObject;
	
		public function ImageItem(imagePath:String, path:String ) {
			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function() {
				dispatchEvent(new Event(Event.COMPLETE));															   
																			   });
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);	

			loader.load(new URLRequest(imagePath + path));

			image = loader;
			
			addChild(image);
			
			closeButton = new BoxCloseButton();	
			
		}
	
		public function show(e:Event):void {
			var imageFadeInTween:GTween = new GTween(this, .5);
			imageFadeInTween.setProperty("alpha", 1);
			imageFadeInTween.ease = Exponential.easeInOut;			
			
			addEventListener(MouseEvent.CLICK, closeEvent);
			
			closeButton.x = image.width - closeButton.width;
			closeButton.y = closeButton.height;
			addChild(closeButton);
		}
		
		public function hide():void {
			try {
				removeChild(closeButton);
			} catch (e:ArgumentError) {
				//not a child. do nothing
			}
			removeEventListener(Event.CLOSE, closeEvent);
		}

		private function closeEvent(e:Event) {
			dispatchEvent(new Event(Event.CLOSE));
		}
		
		public function onError(e:Event):void {
			trace("********************* ERROR ImageItem " + e);
		}				

	}
}