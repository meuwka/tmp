﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	//import fl.transitions.easing.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import com.indelible.utils.*;	
	
	public interface CarouselItem {
		
		function show(e:Event):void;
		function hide():void;
	}
}