﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	import flash.filters.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import com.indelible.utils.*;
	import com.indelible.display.buttons.*;
	
	public class Burst extends FolderImageLoader implements CarouselItem { 
		
		public var imageWidth:int = 100;
		public var imageHeight:int = 100;
		public var skipItem:int;
		public var largeImage:DisplayObject;
		public var border:int = 1;
		
		public var rows:int = 5;
		public var cols:int = 6;
		public var bgColor:int = 0xffffff;
		
		public var closeButton:Sprite;
		
		public function Burst (imagePath:String, folder:String, path:String, count:int, imageWidth:int, imageHeight:int, skipItem:int, largeImage:String, ext:String="jpg") {
			//trace("Burst("+imagePath+", "+folder+", "+path+", "+count+", "+imageWidth+", "+imageHeight+", "+skipItem+", "+largeImage+", "+ext+")");
			
			super(imagePath, folder, path, count);
			this.imageWidth = imageWidth;
			this.imageHeight = imageHeight;
			this.skipItem = skipItem;
			
			if (largeImage != null && largeImage != "") {
				//one more image to load
				super.count = super.count + 1;
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event) {
					//trace("finished loading large image " + this);
					counter = counter + 1;
																						   });
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);		
				
				this.largeImage = loader;
				
				loader.load(new URLRequest(imagePath + "/" + folder + "/" + largeImage));
			}
			
			
			setRowsAndCols(count);
			
			//rec to fill in the width and height
			var _rectangle:Sprite = new Sprite();		
			_rectangle.graphics.lineStyle(0, bgColor, 0);
			_rectangle.graphics.beginFill(bgColor, 1);
			_rectangle.graphics.drawRect(0, 0, cols * (imageWidth+border) - border, rows * (imageHeight+border) - border);
			//_rectangle.alpha = 0;
			this.addChild(_rectangle);					

			closeButton = new BoxCloseButton();

		}
		
		public function show(e:Event):void {
			trace("Burst.show()");
			this.alpha = 1;
			
			for (var i:int =0; i<images.length; i++) {
				//just in case
				images[i].width = imageWidth;
				images[i].height = imageHeight;
				
				images[i].x = i%cols * (imageWidth+border);
				images[i].y = Math.floor(i/cols) * (imageHeight+border);
				
				images[i].alpha = 0;
				var imageFadeInTween:GTween = new GTween(images[i], .8);
				imageFadeInTween.setProperty("alpha", 1);
				imageFadeInTween.delay = i*.01;
				imageFadeInTween.ease = Exponential.easeInOut;			
				
				this.addChild(images[i]);
				
				//shader
				if (i!=skipItem){
					var _rectangle:Sprite = new Sprite();		
					_rectangle.graphics.lineStyle(0, 0x000000, 0);
					_rectangle.graphics.beginFill(0x000000, 1);
					_rectangle.graphics.drawRect(0, 0, imageWidth, imageHeight);
					_rectangle.alpha = 0;
					_rectangle.x = images[i].x;
					_rectangle.y = images[i].y;
					
					var shaderFadeInTween:GTween = new GTween(_rectangle, .8);
					shaderFadeInTween.setProperty("alpha", .6);
					shaderFadeInTween.delay = 1 + i*.01;
					shaderFadeInTween.ease = Exponential.easeInOut;						
					
					this.addChild(_rectangle);	
				} else {

					var glow:GlowFilter = new GlowFilter(0xffffff, 0, 16, 16, 1.5, BitmapFilterQuality.HIGH);
					images[i].filters = [glow];	
					
					var glowTween:GTweenFilter = new GTweenFilter(images[i], .8);
					glowTween.setProperty("alpha", 1);
					glowTween.delay = 1 + i*.01;
					glowTween.ease = Exponential.easeInOut;								
				}
			}
			
			//move skip item to front
			if (skipItem < images.length) {
				this.setChildIndex(images[skipItem],this.numChildren - 1);
				
				//set large image
				trace (largeImage);
				var _rectangle:Sprite = new Sprite();		
				_rectangle.graphics.lineStyle(0, bgColor, 0);
				_rectangle.graphics.beginFill(bgColor, 1);
				_rectangle.graphics.drawRect(0, 0, largeImage.width+border*2, largeImage.height+border*2);
				largeImage.x = border;
				largeImage.y = border;
				_rectangle.addChild(largeImage);
				_rectangle.x = images[skipItem].x;
				_rectangle.y = images[skipItem].y;				
				_rectangle.width = imageWidth;
				_rectangle.height = imageHeight;
				_rectangle.alpha = 0;
				
				this.addChild(_rectangle);
				
				var targetWidth:int = largeImage.width+border*2;
				var targetHeight:int = largeImage.height+border*2;
				var targetX:int = this.width/2 - targetWidth/2;
				var targetY:int = this.height/2 - targetHeight/2;
				
				var largeImageFadeInTween:GTween = new GTween(_rectangle, 1);
				largeImageFadeInTween.setProperty("alpha", 1);
				largeImageFadeInTween.setProperty("width", targetWidth);
				largeImageFadeInTween.setProperty("height", targetHeight);
				largeImageFadeInTween.setProperty("x", targetX);
				largeImageFadeInTween.setProperty("y", targetY);
				largeImageFadeInTween.delay = 2;
				largeImageFadeInTween.ease = Exponential.easeInOut;			
				
			}
			
			addEventListener(MouseEvent.CLICK, closeEvent);
			
			closeButton.x = this.width - closeButton.width;
			closeButton.y = closeButton.height;
			addChild(closeButton);
			
		}
		
		public function hide():void {
			removeEventListener(MouseEvent.CLICK, closeEvent);
		}
		
		public function setRowsAndCols(count:int) {
			var factor1:int = Math.floor(Math.sqrt(count));
			var factor2:int = count/factor1;
			
			for (; factor1*factor2!=count && factor1>0 ; ){
				factor1 = factor1-1;
				factor2 = count/factor1;
				//trace("factoring " + factor1 + " * " + factor2 + " = " + (factor1*factor2));
			}
			
			//factor1 = factor1+1;
			
			rows = factor1;
			cols = factor2;
			
			//trace(rows, cols, count);

		}
		
		private function closeEvent(e:Event) {
			dispatchEvent(new Event(Event.CLOSE));
		}
		
		
	}
}