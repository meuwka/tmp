﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	//import fl.transitions.easing.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import net.stevensacks.preloaders.*;
	import com.indelible.display.preloaders.*;
	import com.indelible.display.buttons.*;
	import com.indelible.utils.*;
	
	public class ItemViewer extends Sprite {
		
		public var imagePath:String = "";
		public var flvPath:String = "";
		protected var image:DisplayObject;
		protected var caption:TextField;
		protected var _rectangle:Sprite;
		protected var tabs:Sprite;
		
		public var minWidth = 0;
		public var minHeight = 0;
		public var maxHeight = 1000;		
		public var visibleWidth = 980;
		public var minX = 0;
		public var minY = 0;
		public var topMargin:int = 0;
		public var preLoader:BoxPreloader = new BoxPreloader();
		public var imageViewAction:Function = imageViewAction2;
		public var bgColor:int = 0xffffff;
		public var border:int = 1;
		
		public var featursIcons:Icons;
		public var cameraIcons:Icons;
		public var deepLinkSection:String;

		public static var MAIN_IMAGE_CLICK:String = "MAIN_IMAGE_CLICK";
		
		public function ItemViewer() {

			//caption setup
			var format:TextFormat = new TextFormat();
			format.font = "Arial";
			format.size = 24;
			format.color = 0xffffff;

			caption = new TextField();
			caption.defaultTextFormat = format;
			caption.autoSize = TextFieldAutoSize.CENTER;
			caption.visible = false;

			addChild(caption);
			
		}

		public function hide() {
			//trace("ItemViewer.hide()");
			if (image != null) {
				
				(image as CarouselItem).hide();
				
				try {
					removeChild(image);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}
				
				image.removeEventListener(Event.CLOSE, mainImageClick);
			}
			
			image = null;
			
			if (_rectangle != null) {
				try {
					removeChild(_rectangle);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}				
			}
			
			if (preLoader != null) {
				try {
					removeChild(preLoader);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}
			}
			
			if (tabs != null) {
				try {
					removeChild(tabs);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}				
				
				for (var i:int = 0; i< tabs.numChildren; i++)
					tabs.getChildAt(i).removeEventListener(MouseEvent.CLICK, tabClick);
				
				
				tabs = null;
			}
			
			if (featursIcons != null) {
				featursIcons.hide();
				try {
					removeChild(featursIcons);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}		
			}
			
			if (cameraIcons != null) {
				cameraIcons.hide();
				try {
					removeChild(cameraIcons);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}		
			}			
			
			caption.visible = false;
			
			
		}

		public function view(item:XML, assetNum:int = 0) {			
			
			if (item.@kind == "image") 
				viewImage(item);
			else if(item.@kind == "burst") 
				viewBurst(item);
			else if(item.@kind == "video") 
				viewVideo(item, assetNum);
				
			image.addEventListener(Event.CLOSE, mainImageClick);
			
			
			var asset:Object = item.assets[assetNum];
			var featureOffset:int = 0;

			if (asset.@camera != null && asset.@camera != "") {
				cameraIcons.show(asset.@camera);
				cameraIcons.visible = false;
				addChild(cameraIcons);
			} else {
				cameraIcons.hide();
			}

			if (asset.@feature != null && asset.@feature != "") {
				featursIcons.show(asset.@feature);
				featursIcons.visible = false;
				addChild(featursIcons);
			} else {
				featursIcons.hide();
			}
			

			
			
			caption.text = item.@copy;
			
		}
		

		
		public function viewImage(item:XML) {
			trace("ItemViewer.viewImage()");
			
			//just to be sure
			hide();
			
			showPreloader();
			
			//images always have exsactly 1 asset
			var asset:Object = item.assets[0];
			
			//trace(asset.@path);
			var loader:ImageItem = new ImageItem(imagePath, asset.@path);
			loader.addEventListener(Event.COMPLETE, handleImageSuccess);

			image = loader;	
			
			
		}
		
		public function viewBurst(item:XML) {
			trace("ItemViewer.viewBurst()");

			//just to be sure
			hide();
			
			showPreloader();
			
			//images always have exsactly 1 asset
			var asset:Object = item.assets[0];
			
			var loader:Burst = new Burst(imagePath, asset.@folder, asset.@path, asset.@max, asset.@w, asset.@h, asset.@skip, asset.@large);
			loader.addEventListener(FolderImageLoader.FINISHED, handleImageSuccess);
			
			image = loader;
			
		}
		
		public function viewVideo(item:XML, assetNum:int) {
			trace("ItemViewer.viewVideo()");

			//just to be sure
			hide();			
			
			showPreloader();
			
			//videos can have multiple assets
			var asset:Object = item.assets[assetNum];
			
			if (item.assets.length() > 1) {
				tabs = new Sprite();
				
				for (var i:int = 0; i < item.assets.length(); i++) {
					var temp:VideoTabs = new VideoTabs(item, i, view);
					temp.setText(item.assets[i].@copy);
					temp.x = i*(temp.width+border);
					
					if (i==assetNum) {
						temp.setActive(true);
					} else {
						temp.addEventListener(MouseEvent.CLICK, tabClick); 
					}
					
					tabs.addChild(temp);
					
				}
				
				tabs.graphics.lineStyle(0, bgColor, 0);
				tabs.graphics.beginFill(bgColor, 1);
				tabs.graphics.drawRect(0, 0, tabs.width, tabs.height);
				
				tabs.alpha = 0;
				
				addChild(tabs);
				
				
			}
			
			image = new VideoItem(flvPath, asset.@path, deepLinkSection, asset.@id);
			
			image.addEventListener(VideoItem.READY, handleImageSuccess);
			
			
		}
		

		public function handleImageSuccess(e:Event):void {
			trace("image load sucess " + image);

			if (image == null) return;

			var finalWidth:int = image.width;
			var finalHeight:int = image.height;

			if (finalHeight > maxHeight) {
				finalWidth = finalWidth * maxHeight / finalHeight;
				finalHeight = maxHeight;
			}

			var targetX:int = minX - (finalWidth-minWidth)/2;	
			var targetY:int = minY - (finalHeight-minHeight)/2;
			if (targetY + finalHeight > minY + minHeight) {
				trace("image should be sqwed up");
				targetY = minY + minHeight - finalHeight;
				if (targetY < topMargin) {
					trace("y goes out the roof");
					targetY = topMargin;
				}
				
			}

			imageViewAction(finalWidth, finalHeight, targetX, targetY);
			
			
			positionTabs(finalWidth, finalHeight, targetX, targetY);
			
			positionCaption(finalWidth, finalHeight, targetX, targetY);
			
			positionCamera(finalWidth, finalHeight, targetX, targetY);
			
			positionFeature(finalWidth, finalHeight, targetX, targetY);
			
			
		}
				
		
		//expand the image itself
		private function imageViewAction1(finalWidth:int, finalHeight:int, targetX:int, targetY:int):void {
			image.width = minWidth;
			image.height = minHeight;
			image.x = minX;
			image.y = minY;
			
			addChild(image);
			
			var tabsTween:GTween;
			if (tabs != null) {
				tabsTween = new GTween(tabs, 1);
				tabsTween.setProperty("alpha",1);
				tabsTween.pause();
				tabsTween.ease = Exponential.easeInOut;
			}			
			
			var widthTween:GTween = new GTween(image, 1);
			widthTween.setProperty("width",finalWidth);
			widthTween.setProperty("x", targetX);
			widthTween.ease = Exponential.easeInOut;
			
			var heightTween:GTween = new GTween(image, 1);
			heightTween.setProperty("height",finalHeight);
			heightTween.setProperty("y",targetY);
			heightTween.ease = Exponential.easeInOut;		
			if (minHeight > 0)
				heightTween.delay = .5;
				
			if (tabsTween != null)
				heightTween.nextTween = tabsTween;			
				
			heightTween.addEventListener(Event.COMPLETE, (image as CarouselItem).show);
							
		}
		
		//expand a white box and then pop image in
		private function imageViewAction2(finalWidth:int, finalHeight:int, targetX:int, targetY:int):void {
			trace("ItemViewer.imageViewAction2("+finalWidth+", "+finalHeight+", "+targetX+", "+targetY+")");
			
			image.width = finalWidth;
			image.height = finalHeight;
			image.x = targetX;
			image.y = targetY;
			
			_rectangle = new Sprite();		
			_rectangle.graphics.lineStyle(0, bgColor, 0);
			_rectangle.graphics.beginFill(bgColor, 1);
			_rectangle.graphics.drawRect(0, 0, minWidth, minHeight);
			//_rectangle.alpha = 0;			
			_rectangle.x = minX;
			_rectangle.y = minY;
			
			addChild(_rectangle);
			addChild(image);
			image.alpha = 0;
			
			var tabsTween:GTween;
			if (tabs != null) {
				tabsTween = new GTween(tabs, 1);
				tabsTween.setProperty("alpha",1);
				tabsTween.pause();
				tabsTween.ease = Exponential.easeInOut;
			}
			
			var widthTween:GTween = new GTween(_rectangle, 1);
			widthTween.setProperty("width",finalWidth + border*2);
			widthTween.setProperty("x", targetX-1);
			widthTween.ease = Exponential.easeInOut;
			
			var heightTween:GTween = new GTween(_rectangle, 1);
			heightTween.setProperty("height",finalHeight + border*2);
			heightTween.setProperty("y",targetY-1);
			heightTween.ease = Exponential.easeInOut;		
			if (minHeight > 0)
				heightTween.delay = .5;
				
			if (tabsTween != null)
				heightTween.nextTween = tabsTween;
				
				
			heightTween.addEventListener(Event.COMPLETE, (image as CarouselItem).show);
							
		}
		
		
		private function positionCaption(finalWidth:int, finalHeight:int, targetX:int, targetY:int) {
			caption.visible = true;
			var tabsOffset:int = 0;
			if (tabs != null)
				tabsOffset = tabs.height;
			caption.y = targetY + finalHeight + 13 + tabsOffset;
			caption.x = targetX + finalWidth/2 - caption.width/2;
			
			//trace(caption.x, caption.y, caption.height);
		}
		
		private function positionTabs(finalWidth:int, finalHeight:int, targetX:int, targetY:int) {
			if (tabs != null) {
				tabs.x = targetX + 8;
				tabs.y = targetY + finalHeight;
			}
		}		

		private function positionCamera(finalWidth:int, finalHeight:int, targetX:int, targetY:int) {
			var usableWidth:int = Math.max(cameraIcons.iconWidth, featursIcons.iconWidth) + 10;
			var tooWideCorrection:int = 0;
			if (targetX + finalWidth + usableWidth > visibleWidth) {
				tooWideCorrection = visibleWidth - (targetX + finalWidth + usableWidth);
				setChildIndex(cameraIcons, numChildren-1);
			}
			
			cameraIcons.x = targetX + finalWidth + usableWidth/2 - cameraIcons.iconWidth/2 + tooWideCorrection;
			
			cameraIcons.y = targetY;
			cameraIcons.visible = true;
		}

		private function positionFeature(finalWidth:int, finalHeight:int, targetX:int, targetY:int) {
			var usableWidth:int = Math.max(cameraIcons.iconWidth, featursIcons.iconWidth) + 10;
			var tooWideCorrection:int = 0;
			if (targetX + finalWidth + usableWidth > visibleWidth) {
				tooWideCorrection = visibleWidth - (targetX + finalWidth + usableWidth);
				setChildIndex(featursIcons, numChildren-1);
			}
			
			featursIcons.x = targetX + finalWidth + usableWidth/2 - featursIcons.iconWidth/2 + tooWideCorrection;
			featursIcons.y = targetY;
			if (cameraIcons.isActive)
				featursIcons.y = targetY + cameraIcons.height + 10;
			featursIcons.visible = true;
		}

		private function showPreloader() {
			if (preLoader != null) {
				try {
					removeChild(preLoader);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}
				
				preLoader.init(minWidth, minHeight, bgColor);
				
				preLoader.x = minX + minWidth/2 - preLoader.width/2;
				preLoader.y = minY + minHeight/2 - preLoader.height/2;
				preLoader.visible = true;				
				addChild(preLoader);
			}
			
		}
		

		//EVENTS
		
		public function mainImageClick(e:Event) {
			trace("ItemViewer.mainImageClick()");
			dispatchEvent(new Event(ItemViewer.MAIN_IMAGE_CLICK));
		}
		
		public function tabClick(e:Event) {
			var tab:VideoTabs = e.target.parent as VideoTabs;
			
			tab.tabAction();
		}
		
		
		public function isThisMyChild(obj:Object):Boolean {
			//trace(obj);
			if (obj == null) return false;
			if (obj == this) return true;
			return isThisMyChild(obj.parent);
		}

		public function onError(e:Event):void {
			trace("********************* ERROR ItemViewer " + e);
		}		
		
	}
}