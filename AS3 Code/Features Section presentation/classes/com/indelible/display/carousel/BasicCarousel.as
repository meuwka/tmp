﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;	
	import com.indelible.utils.*;
	
	public class BasicCarousel extends Sprite {
		
		protected var blockPanel:Sprite;
		
		protected var myImages:Array;
		public var loadedCount:int;
		
		protected var useReflection:Boolean = true;
		protected var relflectionImages = new Array();

		//these should be set by the calling class
		public var imageWidth:int = 100;
		public var imageHeight:int = 100;
		public var visibleWidth:int = 980;
		
		public var itemViewer:ItemViewer;
		
		private var dataStruct:XMLList;
		
		public function init(imagePath:String, images:XMLList, playIntro:Boolean = true) {
			trace("BasicCarousel.init(): loading " + images.length() + " images from " + imagePath);
			
			this.dataStruct = images;
			
			if (itemViewer == null) itemViewer = new ItemViewer();
			
			this.myImages = new Array(images.length());
			
			for (var i:int = 0; i<images.length(); i++) {
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleImageSuccess);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
				//trace ("hi mom " + images[i].@tpath);
				this.myImages[i] = loader;
				loader.load(new URLRequest(imagePath + images[i].@tpath));	
				
			}
		}
		
		protected function intro() {
			
			//first a block to stop the mouse events
			var _rectangle:Sprite = new Sprite();		
			_rectangle.graphics.lineStyle(0, 0x000000, 0);
			_rectangle.graphics.beginFill(0x000000, 1);
			_rectangle.graphics.drawRect(0, 0, visibleWidth, 2000);
			_rectangle.alpha = .4;
			_rectangle.y = -500;
			
			blockPanel.addChild(_rectangle);
			blockPanel.visible = true;
			
			blockPanel.addEventListener(MouseEvent.CLICK, IntroFadeOut);
		}		
		
		protected function IntroFadeOut(e:Event) {
			blockPanel.visible = false;
		}
		
		public function handleImageSuccess(e:Event):void {
			//trace("image load sucess " + this.loadedCount);
			this.loadedCount=this.loadedCount+1;
			
			e.target.content.width = this.imageWidth;
			e.target.content.height = this.imageHeight;
			
			//load reflection if apropriate
			if (useReflection) {
				for (var i:int = 0; i<relflectionImages.length; i++) {
					//trace("%%%% " + e.target.content);
					if ((relflectionImages[i] as ReflectionItem).isMyMaster(e.target.content as DisplayObject)) {
						//trace("reflecting " + e.target);
						(relflectionImages[i] as ReflectionItem).loadFromBitmap(e.target.content);
					}
					
				}
			}
			
		}
		
		protected function getDataForImage(image:Object):XML {
			var i:int = myImages.indexOf(image);
			if (i >= 0)
				return dataStruct[i];
			else return null;
		}
		
		protected function getDataForId(id:String):XML {
			if (id == null || id == "") return null;
			
			var ret:XML;
			
			for (var i:int = 0; i<dataStruct.length() && ret==null; i++) {
				if (dataStruct[i].assets.@id == id)
					ret = dataStruct[i];
			}
			
			return ret;
		}
		
		protected function getImageForData(data:XML):DisplayObject {
			if (data == null)
				return null;
				
			var i:int = -1;
			
			for (var j:int = 0; j<dataStruct.length() && i<0; j++) {
				if (dataStruct[j] == data)
					i = j;
			}
			
			if (i >= 0)
				return myImages[i];
			else return null;
		}
		
		public function onError(e:Event):void {
			trace("********************* ERROR BasicCarousel " + e);
		}
		
	}
}