﻿package com.indelible.display.carousel {
	
	import flash.display.*;
	import flash.utils.*;
	import flash.geom.*;
	import com.indelible.utils.*;
	
	public class ReflectionItem extends Sprite {
		
		protected var image:Sprite;
		protected var master:DisplayObject;
		
		public var reflectionHeight:Number;
		public var reflectionStrength:Number;
		public var reflectionOffsetY:Number;		
		
		
		public function ReflectionItem(master:DisplayObject, reflectionOffsetY:Number = 0, height:Number = 255, strength:Number = .5) {
			this.reflectionHeight = height;
			this.reflectionStrength = strength;
			this.reflectionOffsetY = reflectionOffsetY;
			this.master = master;
		}
		
		public function loadFromBitmap(item:Bitmap) {
			//trace("Reflection.loadFromBitmap() " + item.width + ", " + item.height);
			
			var bitmap:Bitmap = new Bitmap(item.bitmapData);
			bitmap.width = item.width;
			bitmap.height = item.height;
			
			image = new Sprite();
			image.addChild(bitmap);
			
			
			var reflection:Reflection = new Reflection(image, reflectionOffsetY, reflectionHeight, reflectionStrength);
			addChild(reflection);			
		}
		
		public function isMyMaster(master:DisplayObject):Boolean {
			//trace("ReflectionItem.isMyMaster(" + master+") <> " + this.master + " " + (this.master is Loader));
			if (this.master is Loader)
				return (this.master as Loader).content == master;
			else
				return this.master == master;
		}
		
	}
	
}