﻿package com.indelible.display.video.wrapper
{
	import flash.external.ExternalInterface;	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;	
	import flash.net.URLVariables;
	import flash.display.Loader;
	
	public class CloseExternalInterface extends Sprite
	{
		public var debug:Boolean = true;
		public var CloseButton:bt_close;
		
		public function CloseExternalInterface()
		{
			if (debug) trace("CloseExternalInterface.Hi!");
			CloseButton.addEventListener(MouseEvent.CLICK,callJS);
			CloseButton.buttonMode = true;
			
			var loader:Loader = new Loader();
			var flashvar:URLVariables = new URLVariables();
			
			if (loaderInfo.parameters["section"] != null )
			{
				flashvar.section = String(stage.loaderInfo.parameters["section"]);
				if (debug) trace("section = " + flashvar.section);
			}
			if (loaderInfo.parameters["id"] != null )
			{
				flashvar.id = String(stage.loaderInfo.parameters["id"]);				
				if (debug) trace("id = " + flashvar.id);
			}
			
			if (loaderInfo.parameters["flvUrl"] != null )
			{			
				flashvar.flvUrl = loaderInfo.parameters["flvUrl"];
				if (debug) trace("flvUrl = " + flashvar.flvUrl);
			}			
			
			var playerURL:String = "flv_player_g.swf";
			if (loaderInfo.parameters["playerURL"] != null )
			{
				playerURL = String(stage.loaderInfo.parameters["playerURL"]);
				
			}
			
			var request:URLRequest = new URLRequest(playerURL);
			request.data = flashvar;		
			
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
			loader.contentLoaderInfo.addEventListener(Event.INIT,loadComplete);
			loader.x = 0;
			loader.y = 0;
			loader.load(request);		
			addChildAt(loader,0)				;
		}
		
		private function callJS(...List):void
		{
			if (debug) trace ("CloseExternalInterface.callJS.");
			ExternalInterface.call("revertHome");
		}
		
		private function onError(e:Event):void
		{
			trace(e);
		}
		
		private function loadComplete(e:Event):void
		{
			if (debug) trace ("CloseExternalInterface.loadComplete.");
		}
		
	}
}