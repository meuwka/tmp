﻿package com.indelible.display.preloaders {

	import flash.display.*;
	import flash.ui.*;
	import flash.events.*;
	import flash.net.*;		
	import fl.transitions.*;
	//import fl.transitions.easing.*;
	import fl.motion.easing.*;
	import flash.utils.*;
	import flash.geom.*;	
	import com.gskinner.motion.*;
	import fl.motion.easing.*;
	import flash.text.*;
	import net.stevensacks.preloaders.*;
	
	public class BoxPreloader extends Sprite {
		
		var _rectangle:Sprite;
		var spiner:Sprite;
		
		public function BoxPreloader() {
			
		}
		
		public function init(width:int, height:int, color:uint) {
			
			if (_rectangle != null) {
				try {
					removeChild(_rectangle);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}
			}
			
			if (spiner != null) {
				try {
					removeChild(spiner);
				} catch(e:ArgumentError) {
					//child not there. Do nothing.
				}
			}			
			
			_rectangle = new Sprite();		
			_rectangle.graphics.lineStyle(0, color, 1);
			_rectangle.graphics.beginFill(color, 1);
			//trace("&&&&&&&&&&&&&&&&&&&&&&&& " + this.numRows + " * " + this.cellHeight + " = " + (this.numRows * this.cellHeight));
			_rectangle.graphics.drawRect(0, 0, width, height);
			//_rectangle.alpha = 0;			
			
			addChild(_rectangle);
			
			spiner = new CircleSlicePreloader();
			spiner.x = width/2;
			spiner.y = height/2;
			addChild(spiner);
			
			
		}
		
		
		
		
		
	}
}