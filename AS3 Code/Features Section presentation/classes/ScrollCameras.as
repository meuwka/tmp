﻿package
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import flash.net.*;
	
    import com.greensock.*;
    import com.greensock.easing.*;
	
	public class ScrollCameras extends MovieClip {
		var iconsLoaded:int = 0;
		var iconsCount:int;
		var block02:MovieClip;
		//var activeCam:activeCamera;
		var images_path:String = "cameras/";
		var rsStopped:Boolean;
		var lsStopped:Boolean;
		var isContainerLoaded: Boolean = false;
		var isImagesLoaded: Boolean = false;
		var isAllLoaded: Boolean = false;
		var la:LeftArrow;
		var ra:RightArrow;
		var feature: XML;
		const CWIDTH:int = 454;
		const CHEIGHT:int = 124;
		const IMAX:int = 3;
		const IWIDTH:Number = 152;//(CWIDTH + CHID*2)/IMAX;
		const IHEIGHT:int = 124;
		//const CHID:Number = 2;
		
        function ScrollCameras(camerasPath: String):void {
			images_path = camerasPath;
		}

		function create(parent_mc: MovieClip, sourceFeature: XML):void {
			feature = sourceFeature;
			createCamerasCarousel(parent_mc);
		}
		
		function createCamerasCarousel(parent_mc: MovieClip):void {
			block02 = new MovieClip();
			//MASK
			var block02mask:Sprite = new Sprite;
			block02mask.graphics.beginFill(0x1e1e1e);
			block02mask.graphics.drawRect(0,0,CWIDTH,CHEIGHT);
			block02mask.graphics.endFill();
			parent_mc.addChild(block02mask);
			block02.mask = block02mask;  
			parent_mc.addChild(block02);
			
            iconsCount=feature.details.cameras.camera.length();
			for (var i:int=0; i<iconsCount; i++)	{
				iconLoad(feature.details.cameras.camera[i]);
			}			
			if (iconsLoaded > IMAX) { 
				initScroll(parent_mc);
			}
			if (isImagesLoaded || iconsCount==0) {
				this.dispatchEvent(new Event("AllLoaded"));
			} 
			isContainerLoaded = true;
			/*activeCam = new activeCamera();
			activeCam.alpha = 0;
			block02.addChild(activeCam);*/
		}

		function iconLoad(camera:XML):void {
			var item:CameraContainer = new CameraContainer();
			item.x = iconsLoaded * IWIDTH ;
			item.y = 0;
			var l:Loader = new Loader();			
			l.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
			l.load(new URLRequest(images_path + camera.@path));
			l.name = camera.@name;
			//l.contentLoaderInfo.addEventListener(Event.INIT, onIconLoaded);
			l.y = 10;
			item.addChild(l);
			item.CameraText.txt.text = camera.@name;
			item.buttonMode = true;
			item.addEventListener (MouseEvent.CLICK, onIconClick);
			item.addEventListener (MouseEvent.MOUSE_OVER, iconOver);
			item.addEventListener(MouseEvent.MOUSE_OUT, iconOut);
			block02.addChild(item);
			iconsLoaded++;
			if (iconsCount==iconsLoaded) {
				isImagesLoaded = true;
				if (isContainerLoaded) {
					this.dispatchEvent(new Event("AllLoaded"));
				} 
			}
		}
		
		function onIconClick (e:MouseEvent) {
			var link: String;
			for (var i: int = 0; i<feature.details.cameras.camera.length(); i++ ) {
				if (feature.details.cameras.camera[i].@name == e.target.name) {
					link = feature.details.cameras.camera[i].@link;
					break;
				}
			}
		    navigateToURL(new URLRequest(link),"_self");
		}
		
		private function onIconLoaded(e:Event):void	{
			var temp:Bitmap = e.currentTarget.content;
			temp.x = (IWIDTH - temp.width)/2;
			temp.smoothing = true;
		}
		
		// general error catch
		private function onError(e:Event):void
		{
			trace("CameraCarousel.error "+e);
		}
		
		private function initScroll(parent_mc: MovieClip):void {
			lsStopped = false;
			rsStopped = false;
			
			ra = new RightArrow();
			la = new LeftArrow();
			
			la.addEventListener(MouseEvent.MOUSE_DOWN, lsStart);
			la.addEventListener(MouseEvent.MOUSE_OUT, arrowOut);
			la.addEventListener(MouseEvent.MOUSE_OVER, arrowOver);

			la.x = 0;
			la.y = 33.4;
			la.buttonMode = true;
			
			ra.addEventListener(MouseEvent.MOUSE_DOWN, rsStart);
			ra.addEventListener(MouseEvent.MOUSE_OUT, arrowOut);
			ra.addEventListener(MouseEvent.MOUSE_OVER, arrowOver);
			ra.x = 435;
			ra.y = 33.4;
			ra.buttonMode = true;
			
			parent_mc.addChild(ra);
			parent_mc.addChild(la);
		}
		
		function simulateScroll():void {
			if (iconsLoaded <= IMAX) {
				//trace("Scroll camera is null");
			}
			else{
			ra.gotoAndStop(2);
			if ( block02.x + IWIDTH <= -1*block02.getChildAt(0).x )
			{			
				trace("swapIconsRight");
				swapIconsRight();
			}
			var off:Number = -block02.getChildAt(1).x;
			TweenMax.to(block02, 1, {x:off, onComplete: function(){ra.gotoAndStop(1);}});
			}
		}
		
		private function lsStart(e:Event):void {
			lsStopped = false;			
			la.removeEventListener(MouseEvent.MOUSE_DOWN, lsStart);
			la.addEventListener(MouseEvent.MOUSE_UP, lsStop);			
			la.addEventListener(MouseEvent.MOUSE_OUT, lsStop);			
			moveLeftStart();
		}
		
		private function rsStart(e:Event):void {
			rsStopped = false;
			ra.removeEventListener(MouseEvent.MOUSE_DOWN, rsStart);
			ra.addEventListener(MouseEvent.MOUSE_UP, rsStop);			
			ra.addEventListener(MouseEvent.MOUSE_OUT, rsStop);			
			moveRightStart();
		}
		
		private function lsStop(e:Event):void {
			lsStopped = true;
		}
		
		private function rsStop(e:Event):void {
			rsStopped = true;
		}	
		
		private function swapIconsLeft():void {
			var lastIcon = block02.getChildAt(block02.numChildren-1);
			block02.removeChild(lastIcon);			
		    lastIcon.x = block02.getChildAt(0).x-(IWIDTH);
			block02.addChildAt(lastIcon,0);	
		}
	
		private function moveLeftStart():void {
			//lsStopped = false;
			if (block02.x == 0){
				swapIconsLeft()
				}
			//trace(block02.x);
			TweenMax.to(block02, 0.3, {x:block02.x + (IWIDTH)/2, ease:Quad.easeIn, onComplete: function(){
				if (lsStopped) {
			        moveLeftStop();
	            } else {
					moveLeftHold();
	           }
			}});
		}
		
		private function moveLeftHold():void
		{
			if ( block02.x + IWIDTH >= -1*(block02.getChildAt(0).x) ) {
				swapIconsLeft();
			}
			TweenMax.to(block02, 0.13, {x:block02.x + (IWIDTH)/2, onComplete: function(){
						if (lsStopped) {				
				        moveLeftStop();
			            } else {
				        moveLeftHold();
			            }
				}});	
		}
		
		private function moveLeftStop():void
		{
			la.addEventListener(MouseEvent.MOUSE_DOWN, lsStart);
			if ( block02.x + IWIDTH >= -1*(block02.getChildAt(0).x) ) {
				swapIconsLeft();
			}
			var off:Number = (IWIDTH*(Math.ceil(block02.x/IWIDTH)));
			TweenMax.to(block02, 0.3, {x:off, ease:Quad.easeOut, onComplete: function(){
						}});
		}		
		
		// SCROLL RIGHT LOGIC
		private function swapIconsRight():void
		{			
			var firstIcon = block02.getChildAt(0);			
			block02.removeChild(firstIcon);						
			firstIcon.x = block02.getChildAt(block02.numChildren-1).x+(IWIDTH);			
			block02.addChild(firstIcon);	
		}		

		private function moveRightStart():void 
		{
			TweenMax.to(block02, 0.3, {x:block02.x - (IWIDTH)/2, ease:Quad.easeIn, onComplete: function(){
						if (rsStopped) {
				         moveRightStop();
			            } else {
				        moveRightHold();
			            }
				}});	
		}		

		private function moveRightHold():void
		{
			var off:Number = -1*block02.getChildAt(block02.numChildren - 1).x+IWIDTH;
			if (  block02.x + IWIDTH<= -1*block02.getChildAt(0).x )	{
				swapIconsRight();
			}		
			TweenMax.to(block02, 0.13, {x:block02.x - (IWIDTH)/2, onComplete: function(){
						if (rsStopped) {
				         moveRightStop();
			            } else {
				        moveRightHold();
			            }
				}});
		}
		
		private function moveRightStop():void
		{
			ra.addEventListener(MouseEvent.MOUSE_DOWN, rsStart);	
			if ( block02.x + IWIDTH <= -1*block02.getChildAt(0).x )
			{			
				swapIconsRight();
			}
			var off:Number = Math.floor(block02.x/IWIDTH)*IWIDTH;
			TweenMax.to(block02, 0.3, {x:off, ease:Quad.easeOut, onComplete: function(){}});
		}		

		private function arrowOut(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(1);
		}
		
		private function arrowOver(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(2);
		}
		
		private function iconOver(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(2);
		}
		private function iconOut(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(1);
		}
		
	}
}