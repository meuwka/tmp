﻿package
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.display.SpreadMethod;
	import flash.display.InterpolationMethod;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	import gs.OverwriteManager;
	import gs.TweenFilterLite;
	
	public class FullScreenScrollBar extends Sprite
	{
		private var _content:DisplayObjectContainer;
		private var _trackColor:uint;
		private var _grabberColor:uint;
		private var _grabberPressColor:uint;
		private var _gripColor:uint;
		private var _trackThickness:int;
		private var _grabberThickness:int;
		private var _easeAmount:int;
		private var _hasShine:Boolean;
		
		private var _track:Sprite;
		private var _grabber:Sprite;
		private var _grabberGrip1:Sprite;
		private var _grabberGrip2:Sprite;
		private var _grabberEnd1: Sprite;
		private var _grabberEnd2: Sprite;
		
		private var _tW:Number; // Track width
		private var _cW:Number; // Content width
		private var _scrollValue:Number;
		private var _defaultPosition:Number;
		private var _stageW:Number;
		private var _stageH:Number;
		private var _pressed:Boolean = false;
		
		//============================================================================================================================
		public function FullScreenScrollBar(c:DisplayObjectContainer, tc:uint, gc:uint, gpc:uint, grip:uint, tt:int, gt:int, ea:int, hs:Boolean)
		//============================================================================================================================
		{
			_content = c;
			_trackColor = tc;
			_grabberColor = gc;
			_grabberPressColor = gpc;
			_gripColor = grip;
			_trackThickness = tt;
			_grabberThickness = gt;
			_easeAmount = ea;
			_hasShine = hs;
			init();
			OverwriteManager.init();
		}
		
		//============================================================================================================================
		private function init():void
		//============================================================================================================================
		{
			createTrack();
			createGrabber();
			createGrips();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			_defaultPosition = Math.round(_content.x);
			_grabber.x = 0;
		}
		
		//============================================================================================================================
		public function kill():void
		//============================================================================================================================
		{
			stage.removeEventListener(Event.RESIZE, onStageResize);
			stage.removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener);
		}

		//============================================================================================================================
		public function revive():void
		//============================================================================================================================
		{
			stage.addEventListener(Event.RESIZE, onStageResize, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener);
		}
		
		//============================================================================================================================
		private function stopScroll(e:Event):void
		//============================================================================================================================
		{
			onUpListener();
		}
		
		//============================================================================================================================
		private function scrollContent(e:Event):void
		//============================================================================================================================
		{
			var tx:Number;
			var dist:Number;
			var moveAmount:Number;
			
			tx = -((_cW - _tW) * (_grabber.x / _scrollValue));
			dist = tx - _content.x + _defaultPosition;
			moveAmount = dist / _easeAmount;
			_content.x += Math.round(moveAmount);
			
			if (Math.abs(_content.x - tx - _defaultPosition) < 1.5)
			{
				_grabber.removeEventListener(Event.ENTER_FRAME, scrollContent);
				_content.x = Math.round(tx) + _defaultPosition;
			}
			
			positionGrips();
		}
		
		//============================================================================================================================
		public function adjustSize():void
		//============================================================================================================================
		{
			this.y = _stageH - _trackThickness;
			_track.width = _stageW;
			_track.x = 0;
			_tW = _track.width;
			_cW = _content.width + _defaultPosition;
			
			// Set width of grabber relative to how much content
			_grabber.getChildByName("bg").width = Math.ceil((_tW / _cW) * _tW);
			
			// Set minimum size for grabber
			if(_grabber.getChildByName("bg").width < 35) _grabber.getChildByName("bg").width = 35;
			
			if(_hasShine) _grabber.getChildByName("shine").width = _grabber.getChildByName("bg").width;
			
			// If scroller is taller than stage width, set its y position to the very bottom
			if ((_grabber.x + _grabber.getChildByName("bg").width) > _tW) _grabber.x = _tW - _grabber.getChildByName("bg").width;
			
			// If content height is less than stage height, set the scroller x position to 0, otherwise keep it the same
			_grabber.x = (_cW < _tW) ? 0 : _grabber.x;
			
			// If content width is greater than the stage width, show it, otherwise hide it
			this.visible = (_cW + 8 > _tW);
			
			// Distance left to scroll
			_scrollValue = _tW - _grabber.getChildByName("bg").width;
			
			_content.x = Math.round(-((_cW - _tW) * (_grabber.x / _scrollValue)) + _defaultPosition);
			
			positionGrips();
			
			if(_content.width < stage.stageWidth) { stage.removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener); } else { stage.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener); }
		}
		
		//============================================================================================================================
		private function positionGrips():void
		//============================================================================================================================
		{
			_grabberGrip1.x = _grabber.getChildByName("bg").x + 10;
			_grabberGrip2.x = _grabber.getChildByName("bg").width - _grabberGrip2.width - 10;
			
			_grabberEnd1.x = _grabber.getChildByName("bg").x;
			_grabberEnd2.x = _grabber.getChildByName("bg").width - _grabberEnd2.width;
		}

		//============================================================================================================================
		// CREATORS
		//============================================================================================================================
		
		//============================================================================================================================
		private function createTrack():void
		//============================================================================================================================
		{
			_track = new Sprite();
			var t:Sprite = new Sprite();
			var colors: Array = [0x1E1E1E, 0x474747];
			var alphas: Array = [1, 1];
			var ratios: Array = [0, 255];
			var m: Matrix = new Matrix();
			m.createGradientBox(_trackThickness,_trackThickness, Math.PI / 2);
			t.graphics.beginGradientFill(GradientType.LINEAR, colors, alphas, ratios, m, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB); 
			t.graphics.drawRect(0, 0, _trackThickness, _trackThickness);
			t.graphics.endFill();
			_track.addChild(t);
			addChild(_track);
		}
		
		//============================================================================================================================
		private function createGrabber():void
		//============================================================================================================================
		{
			_grabber = new Sprite();
			var t:Sprite = new Sprite();
			// new gradient
			var colors: Array = [0xFF4438, 0xA91A0A];
			var alphas: Array = [1, 1];
			var ratios: Array = [0, 255];
			var m: Matrix = new Matrix();
			m.createGradientBox(_grabberThickness,_grabberThickness, Math.PI / 2);
			t.graphics.beginGradientFill(GradientType.LINEAR, colors, alphas, ratios, m, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB); 
			t.graphics.drawRect(0, 0, _grabberThickness, _grabberThickness);
			t.graphics.endFill();
			
			t.graphics.beginFill(0xf74243,1);
			t.graphics.drawRect(0, 0, _grabberThickness, 1);
			t.graphics.drawRect(0, _grabberThickness-1, _grabberThickness, 1);
			t.graphics.endFill();

			t.name = "bg";
			_grabber.addChild(t);
			
			if(_hasShine)
			{
				var shine:Sprite = new Sprite();
				var sg:Graphics = shine.graphics;
				sg.beginFill(0xffffff, 0.15);
				sg.drawRect(0, 0, _trackThickness, Math.ceil(_trackThickness/2));
				sg.endFill();
				shine.y = Math.floor(_trackThickness/2);
				shine.name = "shine";
				_grabber.addChild(shine);
			}
			
			addChild(_grabber);
		}
		
		//============================================================================================================================
		private function createGrips():void
		//============================================================================================================================
		{
			_grabberGrip1 = createGrabberGrip();
			_grabber.addChild(_grabberGrip1);
			_grabberGrip1.x = 5;
			
			_grabberGrip2 = createGrabberGrip();
			_grabber.addChild(_grabberGrip2);
			_grabberGrip2.x = _grabber.width - 20;
			
			_grabberEnd1 = new Sprite;
			_grabberEnd1.graphics.beginFill(0xf74243,1);
			_grabberEnd1.graphics.drawRect(0,1,1,_grabberThickness-1);
			_grabberEnd1.graphics.endFill();
			_grabberEnd1.graphics.beginFill(0x000000,1);
			_grabberEnd1.graphics.drawRect(0,0,1,1);
			_grabberEnd1.graphics.drawRect(0,_grabberThickness-1,1,1);
			_grabberEnd1.graphics.endFill();
			_grabber.addChild(_grabberEnd1);

			_grabberEnd2 = new Sprite;
			_grabberEnd2.graphics.beginFill(0xf74243,1);
			_grabberEnd2.graphics.drawRect(0,1,1,_grabberThickness-1);
			_grabberEnd2.graphics.endFill();
			_grabberEnd2.graphics.beginFill(0x000000,1);
			_grabberEnd2.graphics.drawRect(0,0,1,1);
			_grabberEnd2.graphics.drawRect(0,_grabberThickness-1,1,1);
			_grabberEnd2.graphics.endFill();
			_grabber.addChild(_grabberEnd2);
		}
		
		//============================================================================================================================
		private function createGrabberGrip():Sprite
		//============================================================================================================================
		{
			var h:int = 14;
			var yp:int = (_grabberThickness - h) / 2;
			var t:Sprite = new Sprite();
			t.graphics.beginFill(0xf74243, 1);
			t.graphics.drawRect(0, yp, 1, h);
			t.graphics.drawRect(0 + 2, yp, 1, h);
			t.graphics.drawRect(0 + 4, yp, 1, h);
			t.graphics.drawRect(0 + 6, yp, 1, h);
			t.graphics.drawRect(0 + 8, yp, 1, h);
			t.graphics.endFill();
			t.graphics.beginFill(0xbd2223, 1);
			t.graphics.drawRect(0 + 1, yp, 1, h);
			t.graphics.drawRect(0 + 3, yp, 1, h);
			t.graphics.drawRect(0 + 5, yp, 1, h);
			t.graphics.drawRect(0 + 7, yp, 1, h);
			t.graphics.drawRect(0 + 9, yp, 1, h);
			t.graphics.endFill();
			return t;
		}
	
		//============================================================================================================================
		// LISTENERS
		//============================================================================================================================
		
		//============================================================================================================================
		private function mouseWheelListener(me:MouseEvent):void
		//============================================================================================================================
		{
			return;
			var d:Number = me.delta;
			if (d > 0)
			{
				if ((_grabber.x - (d * 4)) >= 0)
				{
					_grabber.x -= d * 4;
				}
				else
				{
					_grabber.x = 0;
				}
				
				if (!_grabber.willTrigger(Event.ENTER_FRAME)) _grabber.addEventListener(Event.ENTER_FRAME, scrollContent);
			}
			else
			{
				if (((_grabber.x + _grabber.width) + (Math.abs(d) * 4)) <= stage.stageWidth)
				{
					_grabber.x += Math.abs(d) * 4;
				}
				else
				{
					_grabber.x = stage.stageWidth - _grabber.width;
				}
				if (!_grabber.willTrigger(Event.ENTER_FRAME)) _grabber.addEventListener(Event.ENTER_FRAME, scrollContent);
			}
		}
		
		//============================================================================================================================
		private function onDownListener(e:MouseEvent):void
		//============================================================================================================================
		{
			_pressed = true;
			_grabber.startDrag(false, new Rectangle(0, 0, _stageW - _grabber.getChildByName("bg").width, 0));
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveListener, false, 0, true);
			TweenFilterLite.to(_grabber.getChildByName("bg"), 0.5, { tint:_grabberPressColor } );
		}
		
		//============================================================================================================================
		private function onUpListener(e:MouseEvent = null):void
		//============================================================================================================================
		{
			if (_pressed)
			{
				_pressed = false;
				_grabber.stopDrag();
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveListener);
				TweenFilterLite.to(_grabber.getChildByName("bg"), 0.5, { tint:null } );
			}
		}
		
		//============================================================================================================================
		private function onMouseMoveListener(e:MouseEvent):void
		//============================================================================================================================
		{
			e.updateAfterEvent();
			if (!_grabber.willTrigger(Event.ENTER_FRAME)) _grabber.addEventListener(Event.ENTER_FRAME, scrollContent, false, 0, true);
		}
		
		//============================================================================================================================
		private function onTrackClick(e:MouseEvent):void
		//============================================================================================================================
		{
			var p:int;
			var s:int = 150;
			
			p = Math.ceil(e.stageX);
			if(p < _grabber.x)
			{
				if(_grabber.x < _grabber.width)
				{
					TweenFilterLite.to(_grabber, 0.5, {x:0, onComplete:reset, overwrite:1});
				}
				else
				{
					TweenFilterLite.to(_grabber, 0.5, {x:"-150", onComplete:reset});
				}
				
				if(_grabber.x < 0) _grabber.x = 0;
			}
			else
			{
				if((_grabber.x + _grabber.width) > (_stageW - _grabber.width))
				{
					TweenFilterLite.to(_grabber, 0.5, {x:_stageW - _grabber.width, onComplete:reset, overwrite:1});
				}
				else
				{
					TweenFilterLite.to(_grabber, 0.5, {x:"150", onComplete:reset});
				}
				
				if(_grabber.x + _grabber.getChildByName("bg").width > _track.width) _grabber.x = stage.stageWidth - _grabber.getChildByName("bg").width;
			}
			
			function reset():void
			{
				if(_grabber.x < 0) _grabber.x = 0;
				if(_grabber.x + _grabber.getChildByName("bg").width > _track.width) _grabber.x = stage.stageWidth - _grabber.getChildByName("bg").width;
			}
			
			_grabber.addEventListener(Event.ENTER_FRAME, scrollContent, false, 0, true);
		}
		
		//============================================================================================================================
		private function onAddedToStage(e:Event):void
		//============================================================================================================================
		{
			stage.addEventListener(Event.MOUSE_LEAVE, stopScroll);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener);
			stage.addEventListener(Event.RESIZE, onStageResize, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onUpListener, false, 0, true);
			_grabber.addEventListener(MouseEvent.MOUSE_DOWN, onDownListener, false, 0, true);
			_grabber.buttonMode = true;
			_track.addEventListener(MouseEvent.CLICK, onTrackClick, false, 0, true);
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_stageW = stage.stageWidth;
			_stageH = stage.stageHeight;
			adjustSize();
		}
		
		//============================================================================================================================
		private function onStageResize(e:Event):void
		//============================================================================================================================
		{
			_stageW = stage.stageWidth;
			_stageH = stage.stageHeight;
			adjustSize();
		}
		
	}
}