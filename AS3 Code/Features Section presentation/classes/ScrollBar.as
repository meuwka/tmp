﻿package {
	import flash.display.*;
	import flash.text.*;
	import flash.events.*;
	import flash.utils.*;
	import flash.geom.*;

	// USAGE:
	// var theTextField:TextField = new TextField( ); 
	// someContainer.addChild(theTextField); 
	// var scroll bar:Scroll Bar = new ScrollBar(theTextField); 
	// someContainer.addChild(scrollbar);
	
	public class ScrollBar extends Sprite {
		private var t:TextField;
		private var tHeight:Number;
		private var scrollTrack:Sprite;
		private var scrollThumb:Sprite;
		private var scrollbarTrack:int=1;
		private var scrollbarWidth:int=11;
		private var thumbHeight:int = 30;
		private var dragging:Boolean=false;
		private var changed:Boolean=false;

		public function ScrollBar(textfield:TextField) {

			t = textfield;
			tHeight = t.height;
			var _thickness:int = 1;
			
			// create scrollTrack
			scrollTrack = new Sprite();
			scrollTrack.graphics.lineStyle();
			scrollTrack.graphics.beginFill(0xFFFFFF);
			scrollTrack.graphics.drawRect(0, 0, 1, 1);
			addChild(scrollTrack);

			// create scrollThumb
			scrollThumb = new Sprite();
			var st:Sprite = new Sprite();
			// new gradient
			var colors: Array = [0x8A170A,0xFF4438];
			var alphas: Array = [1, 1];
			var ratios: Array = [0, 255];
			var m: Matrix = new Matrix();
			m.createGradientBox(_thickness,_thickness, Math.PI/4*7 + Math.PI/8);
			st.graphics.beginGradientFill(GradientType.LINEAR, colors, alphas, ratios, m, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB); 
			st.graphics.drawRect(0, 0, _thickness, _thickness);
			st.graphics.endFill();
			
			st.graphics.beginFill(0xFF4438,1);
			st.graphics.drawRect(0, 0, _thickness, 1);
			st.graphics.drawRect(0, _thickness-1,_thickness, 1);
			st.graphics.endFill();

			scrollThumb.addChild(st);
			addChild(scrollThumb);
			scrollThumb.buttonMode = true;

			t.addEventListener(Event.SCROLL, scrollListener);
			scrollThumb.addEventListener(MouseEvent.MOUSE_DOWN,mouseDownListener);

			var stageDetector:StageDetector=new StageDetector(this);
			stageDetector.addEventListener(StageDetector.ADDED_TO_STAGE, 
			                                     addedToStageListener);
			stageDetector.addEventListener(StageDetector.REMOVED_FROM_STAGE,
			                                     removedFromStageListener);

			addEventListener(Event.ENTER_FRAME, enterFrameListener);
			changed=true;
		}

		private function addedToStageListener(e:Event):void {
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
		}

		private function removedFromStageListener(e:Event):void {
			stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
		}

		private function enterFrameListener(e:Event):void {
			if (t.height!=tHeight) {
				changed=true;
				tHeight=t.height;
				if (dragging) {
					scrollThumb.stopDrag();
					dragging=false;
				}
			}
			if (changed) {
				updateScrollbar();
				changed=false;
			}
		}

		private function scrollListener(e:Event):void {
			if (t.scrollV>t.maxScrollV) {
				return;
			}
			if (! dragging) {
				changed=true;
			}
		}

		public function updateScrollbar():void {
			scrollTrack.x=t.x+t.width+10;
			scrollTrack.y=t.y;
			scrollTrack.height=t.height;
			scrollTrack.width=scrollbarTrack;

			var numVisibleLines:int = t.bottomScrollV - (t.scrollV-1);
			if (numVisibleLines<t.numLines) {
				scrollThumb.visible=true;
				
				// size of scrollThumb
				scrollThumb.height=thumbHeight;
				scrollThumb.width=scrollbarWidth;

				// position 
				scrollThumb.x=t.x+t.width+5;
				scrollThumb.y = t.y + (scrollTrack.height-scrollThumb.height) 
				                        * ((t.scrollV-1)/(t.maxScrollV-1));
			} else {
				scrollThumb.visible=false;
				scrollTrack.visible=false;
			}
		}

		public function synchTextToScrollThumb():void {
			var scrollThumbMaxY:Number=t.height-scrollThumb.height;
			var scrollThumbY:Number=scrollThumb.y-t.y;
			t.scrollV = Math.round(t.maxScrollV 
			                               * (scrollThumbY/scrollThumbMaxY));
		}

		private function mouseDownListener(e:MouseEvent):void {
			var bounds:Rectangle = new Rectangle(t.x+t.width+6, 
			                                       t.y,
			                                       0, 
			                                       t.height-scrollThumb.height);
			scrollThumb.startDrag(false, bounds);
			dragging=true;
		}

		private function mouseUpListener(e:MouseEvent):void {
			if (dragging) {
				synchTextToScrollThumb();
				scrollThumb.stopDrag();
				dragging=false;
			}
		}

		private function mouseMoveListener(e:MouseEvent):void {
			if (dragging) {
				synchTextToScrollThumb();
			}
		}
	}
}