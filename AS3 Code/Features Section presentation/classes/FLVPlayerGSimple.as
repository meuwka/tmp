﻿package
{
	import fl.video.VideoScaleMode;
	import fl.video.VideoEvent;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;	
	import flash.events.MouseEvent;	
	import flash.display.Bitmap;		
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import com.gskinner.motion.GTween;
	import fl.motion.easing.Sine;		
	import flash.net.NetStream;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import fl.video.FLVPlayback;
	import fl.video.VideoEvent;
	import fl.video.MetadataEvent;
	import flash.external.ExternalInterface;	
	
	public class FLVPlayerGSimple extends MovieClip
	{
		public var debug:Boolean = true;
		
		public var flvwidth:Number = 0;
		public var flvheight:Number = 0;
		private var flvPlayer:FLVPlayback = new FLVPlayback();
		private var _fadeDuration:Number;
		private var _duration:Number;
		private var _timer:Timer;
		private var canvas:Sprite;
		private var screen:Sprite;
		private var screenLock:Boolean = false;
		private var seekLock:Boolean = false;
		private var screen_Audio:Audio = new Audio();
		private var screen_AudioHandle:AudioHandle = new AudioHandle();		
		private var screen_AudioRoll:AudioRoll = new AudioRoll();				
		private var screen_BigPlay:BigPlay = new BigPlay();				
		private var screen_Pause:Pause = new Pause();
		private var screen_Play:Play = new Play();		
		private var screen_AudioHandleRange:Sprite = new Sprite;
		private var faded:Boolean = false;
		public var flvURL:String = "20051210-w50s.flv";
		public var timerInterval:int = 100;
		public var volume:Number = 1;
		public var autoplay:Boolean = true;
		public var SECTION:String = "";
		public var ID:String = "";
		public var progr:Sprite = new Sprite();
		
		
		/* FLVPlayerG needs the FLVPlayback component to be on the main swf's stage */

		
		public function FLVPlayerGSimple(...List)
		{			
			if (debug) trace("FLVPlayerG.v33.Hi!");			
			
			if (loaderInfo.parameters["section"] != null )
			{
				SECTION = String(loaderInfo.parameters["section"]);
				if (debug) trace("section = " + SECTION);
			}
			if (loaderInfo.parameters["id"] != null )
			{
				ID = String(loaderInfo.parameters["id"]);				
				if (debug) trace("id = " + ID);				
			}
			
			if (loaderInfo.parameters["flvUrl"] != null )
			{				
				flvURL = String(loaderInfo.parameters["flvUrl"]);			
				if (debug) trace("Parameter found, using " + flvURL);
				flvPlayer.scaleMode = VideoScaleMode.NO_SCALE;
				flvPlayer.volume = volume;
				flvPlayer.autoPlay = autoplay;
				flvPlayer.addEventListener(MetadataEvent.METADATA_RECEIVED,onMetaData2);
				flvPlayer.addEventListener(VideoEvent.STATE_CHANGE,stateTrack);
				flvPlayer.addEventListener(VideoEvent.COMPLETE,onComplete);
				
				flvPlayer.source = flvURL;				
			}
		}
		
		public function setDims(width:Number,height:Number):void
		{
			flvwidth = width;
			flvheight = height;
		}
		
		public function setSource(src:String):void
		{
			flvPlayer.scaleMode = VideoScaleMode.NO_SCALE;
			flvPlayer.volume = volume;
			flvPlayer.autoPlay = autoplay;
			flvPlayer.addEventListener(MetadataEvent.METADATA_RECEIVED,onMetaData2);
			flvPlayer.addEventListener(VideoEvent.STATE_CHANGE,stateTrack);
			flvPlayer.addEventListener(VideoEvent.COMPLETE,onComplete);
			flvURL = src;
			flvPlayer.source = src;	
			
		}
		
		private const PLAY:int = 1;
		private const PAUSE:int = 2;
		private const STOP:int = 3;
		public var gState:int;
		
		// Events Query ( old onNetStatus Logic )
		private function onComplete(e:VideoEvent):void
		{
			if (debug) trace("FLVPlayerG.onComplete.");
				gState = STOP;
				gPause();
				
				new GTween(seekCur,timerInterval/1000,{width:seekCur.range},
									{});
		}
		
		private function stateTrack(e:VideoEvent):void
		{
			if (debug) trace("flvPlayer state = " + e.state + " / " + gState);
			if (e.state == "playing") 
			{
				if (gState == PAUSE) 
				{
					trace("gState = PAUSE");
					gPause();
				}
				else
				{
					trace("gState = PLAY");					
					gState = PLAY;								
					screenLock = false;
					fadeOut(screen_BigPlay,.2);
					fadeOut(screen_Play,.2);
					fadeIn(screen_Pause,1,.1);
					_fadeDuration = 0;
				}
			}
			if (e.state == "paused")
			{
				if (gState == PLAY)
				{
					gPlay();
				}
			}

		}
		
		private var init:Boolean = true;	
		public var loaded:Boolean = false;
		private function onMetaData2(...List):void
		{
			loaded = true;
			flvPlayer.autoRewind = true;
			if (debug) trace("FLVPlayerG.onMetaData2.");
			_duration = flvPlayer.metadata.duration;
			if (init) gInit();
			
			dispatchEvent(new Event(MetadataEvent.METADATA_RECEIVED));
			if (autoplay)
				gPlay();
		}
		
		private var _counter:int = 0;
		private var seekCur:MovieClip = new MovieClip();
		private var seekHit:Sprite = new Sprite();
		private function gInit():void
		{
			if (debug) trace("FLVPlayerG.gInit.--");
			init = false;
			// movie has started	
			if (flvwidth == 0) flvwidth = flvPlayer.metadata.width;  
			if (flvheight == 0) flvheight = flvPlayer.metadata.height;			
			
			canvas = new Sprite;
			canvas.graphics.beginFill(0xffffff);
			canvas.graphics.drawRect(0,0,flvwidth,flvheight);
			canvas.graphics.endFill();					
			canvas.x = 0;
			canvas.y = 0;
			canvas.alpha = 1;
			addChild(canvas);
			
			flvPlayer.x = 0;
			flvPlayer.y = 0;
			
			canvas.addChild(flvPlayer);
			flvPlayer.setSize(flvwidth,flvheight);
			
			// this is the recommended size for this flvplayer
			var swidth = 663;
			var sheight = 497;
						
			screen = new Sprite;			
			
			// screen rectangle
			var srect:Sprite = new Sprite;
			srect.graphics.beginFill(0xffffff);
			srect.graphics.drawRect(0,0,swidth,sheight);
			srect.graphics.endFill();
			srect.alpha = 0;
			/// give it the fadein events
			// height 41
			var crect:Sprite = new Sprite;
			crect.graphics.beginFill(0x3b3b3b);
			crect.graphics.drawRect(0,0,swidth,41);
			crect.graphics.endFill();
			crect.alpha = .7;			
			crect.y = sheight - crect.height;

			screen.addChild(srect);						
			screen.addChild(crect);
			
			
			screen.addChild(screen_BigPlay);			
			screen_BigPlay.x = screen.width/2 - screen_BigPlay.width/2;
			screen_BigPlay.y = screen.height/2 - screen_BigPlay.height/2;			
			screen_BigPlay.buttonMode = true;
			screen_BigPlay.addEventListener(MouseEvent.CLICK,gPlay);
					
			screen.addChild(screen_Play);
			screen_Play.x = 11;
			screen_Play.y =	sheight - crect.height + (crect.height - screen_Play.height)/2;		
			screen_Play.addEventListener(MouseEvent.CLICK,gPlay);			
			screen_Play.buttonMode = true;
			
			screen.addChild(screen_Pause);		
			screen_Pause.x = 11;
			screen_Pause.y = sheight - crect.height + (crect.height - screen_Pause.height)/2;	
			screen_Pause.addEventListener(MouseEvent.CLICK,gPause);			
			screen_Pause.buttonMode = true;
			
			screen.addChild(screen_Audio);
			screen_Audio.x = swidth	- 127;			
			screen_Audio.y = sheight - crect.height + (crect.height - screen_Audio.height)/2;
			screen_AudioRoll.y = (screen_Audio.height - screen_AudioRoll.height);// + 7;
			screen_AudioRoll.x = -2;//-6;
			screen_Audio.addChild(screen_AudioRoll);

			screen_AudioRoll.addChild(screen_AudioHandle);
			screen_AudioRoll.addEventListener(MouseEvent.ROLL_OVER,audioRollOver);
			screen_AudioHandle.x = (screen_AudioRoll.width - screen_AudioHandle.width)/2;
			screen_AudioHandleRange.y =  screen_AudioHandle.height/2 -2.5;
			screen_AudioHandleRange.x = (screen_AudioRoll.width - screen_AudioHandle.width)/2;;
			screen_AudioHandleRange.graphics.beginFill(0xffffff);
			screen_AudioHandleRange.graphics.drawRect(0,0,screen_AudioHandle.width,43);
			screen_AudioHandleRange.graphics.endFill();
			screen_AudioHandleRange.alpha = 0;
			screen_AudioHandleRange.mouseChildren = true;
			screen_AudioHandleRange.buttonMode = true;			
			
			
			screen_AudioHandle.y = 3;//3;//range - 35; (32 range)
			screen_AudioHandleRange.addEventListener(MouseEvent.MOUSE_DOWN, audioMouseDown);
			screen_AudioRoll.alpha = 0;
			screen_AudioRoll.addChild(screen_AudioHandleRange);
			screen_Audio.mouseChildren = true;
			screen_AudioRoll.buttonMode = false;
			screen_Audio.buttonMode = false;			
			
			//seekbar
			var seekrect:Sprite = new Sprite();
			seekrect.graphics.beginFill(0xc1c1c1);
			seekrect.graphics.drawRect(0,0,444,3);
			seekrect.graphics.endFill();
			seekrect.alpha = 1;
			seekrect.x = 61;
			seekrect.y = screen.height - 23;
			screen.addChild(seekrect);
			seekCur.range = 444;
			//seekCur.graphics.beginFill(0xffffff);
			seekCur.graphics.beginFill(0xff0000);
			seekCur.graphics.drawRect(0,0,seekCur.range,3);
			seekCur.graphics.endFill();
			seekCur.alpha = 1;
			seekCur.x = 59;
			seekCur.y = screen.height - 23;
			seekCur.width = 0;
			screen.addChild(seekCur)
			
			// 10 radius/diameter?? box
			var radius:int = 10;
			seekHit.graphics.beginFill(0xffffff);
			seekHit.graphics.drawRect(0,0,444 + radius,3 + radius);
			seekHit.x = seekCur.x - radius/2;
			seekHit.y = seekCur.y - radius/2;
			seekHit.alpha = 0;			
			seekHit.buttonMode = true;
			seekHit.addEventListener(MouseEvent.MOUSE_DOWN, seekMouseDown);
			screen.addChild(seekHit);
			
			
			
			
			
			progr.graphics.beginFill(0xff0000);
			progr.graphics.drawRect(0,0,2,3);
			progr.graphics.endFill();
			progr.x = 61;
			progr.y = screen.height - 23;
			screen.addChild(progr);
			
			
			
			screen.scaleX = flvwidth/screen.width;
			screen.scaleY = flvheight/screen.height;	
			screen.alpha = 1;
			mouseChildren = true;
			addEventListener(MouseEvent.MOUSE_MOVE,gRollOver);
			addEventListener(MouseEvent.ROLL_OUT,gRollOut);			
			addEventListener(MouseEvent.ROLL_OUT,audioRollOut);						
			addChild(screen);			
			
			
			
			
			
			
			
			
			
			
		}
		private var seekMousePos:int;
		private function seekMouseDown(...List):void
		{
			if (debug) trace("FLVPlayerG.seekMouseDown.--");
			seekLock = screenLock = true;
			flvPlayer.pause();			
			_timer.stop();
			_fadeDuration = 0;			
			seekMouseMove();
			seekHit.addEventListener(MouseEvent.MOUSE_MOVE,seekMouseMove);
			// move to cursor location
			seekHit.addEventListener(MouseEvent.MOUSE_UP,seekMouseUp);
			seekHit.addEventListener(MouseEvent.MOUSE_OUT,seekMouseUp);	
			
			
		}
		
		
		private function seekMouseMove(...List):void
		{
			//if (debug) trace("FLVPlayerG.seekMouseMove.");
			seekMousePos = seekHit.mouseX-5;
			
			if (seekMousePos < 0) seekMousePos = 0;
			if (seekMousePos > 444) seekMousePos = 444;
			new GTween(seekCur,.1,{width:seekMousePos},
					   {ease:Sine.easeOut});
			 
		}
				
		private function seekMouseUp(...List):void
		{
			if (debug) trace("FLVPlayerG.seekMouseUp++");
			
			seekLock = false;
			
			var offSet:Number = (seekMousePos*_duration)/seekCur.range;//( seekMousePos / seekCur.range ) * _duration;// - _stream.time;
			flvPlayer.seek(offSet);
			if (gState == PLAY)	
			{
				gPlay();
			}

			offSet = null;
			seekMousePos = null;

			_timer.start();
			seekHit.removeEventListener(MouseEvent.MOUSE_MOVE,seekMouseMove);
			seekHit.removeEventListener(MouseEvent.MOUSE_UP,seekMouseUp);
			seekHit.removeEventListener(MouseEvent.MOUSE_OUT,seekMouseUp);			
		}
				
		
		private function audioMouseDown(...List):void
		{
			if (debug) trace("FLVPlayerG.audioMouseDown.");
			screen_AudioHandleRange.addEventListener(MouseEvent.MOUSE_MOVE,audioMouseMove);
			// move to cursor location
			audioMouseMove();
			screen_AudioHandleRange.addEventListener(MouseEvent.MOUSE_UP,audioMouseUp);
			screen_AudioHandleRange.addEventListener(MouseEvent.MOUSE_OUT,audioMouseUp);
		}
		
		private function audioMouseMove(...List):void
		{
			if (debug) trace("FLVPlayerG.audioMouseMove.");
			var temp:int = screen_AudioHandleRange.mouseY;
			if (temp < 3) temp = 3;
			if (temp >37) temp = 37;

			//trace(temp);
			new GTween(screen_AudioHandle,.2,{y:temp},
					   {ease:Sine.easeOut});
				
			var newVol:Number = 1-(temp-3)/(34);
			//trace(newVol);
			flvPlayer.volume = newVol;
			temp = null;
			newVol = null;
			
		}
		

		
		private function audioMouseUp(...List):void
		{
			if (debug) trace("FLVPlayerG.audioMouseUp");
			volume = flvPlayer.volume;
			screen_AudioHandleRange.removeEventListener(MouseEvent.MOUSE_MOVE,audioMouseMove);
			screen_AudioHandleRange.removeEventListener(MouseEvent.MOUSE_UP,audioMouseUp);
			screen_AudioHandleRange.removeEventListener(MouseEvent.MOUSE_OUT,audioMouseUp);			
		}
		
		public function audioRollOver(...List):void
		{
			if (debug) trace("FLVPlayerG.audioRollOver.");
			new GTween(screen_AudioRoll, .8, {alpha:1}, 
						{ease:Sine.easeIn,completeListener:audioRollOverComplete});												
		}
		
		private function audioRollOverComplete(...List):void
		{
			screen_AudioRoll.addEventListener(MouseEvent.MOUSE_OUT,audioRollOut);
			if (debug) trace("FLVPlayerG.audioRollOverComplete.");			
		}
		
		public function audioRollOut(...List):void
		{
			if (debug) trace("FLVPlayerG.audioRollOut.");
			fadeOut(screen_AudioRoll,.8);
			screen_Audio.addEventListener(MouseEvent.MOUSE_OVER,audioRollOver);				
		}
		
		private function audioRollOutComplete(...List):void
		{
			if (debug) trace("FLVPlayerG.audioRollOutComplete.");
			screen_Audio.addEventListener(MouseEvent.MOUSE_OVER,audioRollOver);	
		}
									
		public function gRollOver(...List):void
		{
			screenLock = true;
			if (faded) screenFadeIn();
			_fadeDuration = 0;
		}		
		
		public function gRollOut(...List):void
		{
			if (debug) trace("FLVPlayerG.gRollOut.");
			screenLock = false;
			_fadeDuration = 0;		
		}				
		
		public function gStop(...List):void
		{
			if (debug) trace("FLVPlayerG.gStop.");
			if  (_timer != null) _timer.stop();
			flvPlayer.stop();
			seekCur.width = 0;
			screenFadeIn();		
			screenLock = true;
			fadeIn(screen_Play,1,.2);
			fadeIn(screen_BigPlay,1,.2);			
			fadeOut(screen_Pause,.1);		
			gState = STOP;
			
		}
		
		public function gPause(...List):void
		{	
			if (debug) trace("FLVPlayerG.gPause.");
			gState = PAUSE;
				new GTween(flvPlayer,.2,{volume:0},{ease:Sine.easeIn});
				if (flvPlayer != null) flvPlayer.pause();
				gState = PAUSE;				
			if  (_timer != null) _timer.stop();			
			screenFadeIn();		
			screenLock = true;
			fadeIn(screen_Play,1,.2);
			fadeIn(screen_BigPlay,1,.2);			
			fadeOut(screen_Pause,.1);
		}
		
		public function gPlay(...List):void
		{
			if (debug) trace("FLVPlayerG.gPlay.");	
			else if (gState == STOP)
			{
				seekCur.width = 0;
			}			
			gState = PLAY;
			screenLock = false;
			fadeOut(screen_BigPlay,.2);
			fadeOut(screen_Play,.2);
			fadeIn(screen_Pause,1,.1);
			_fadeDuration = 0;
			flvPlayer.play();			
			_timer = new Timer(timerInterval);
			_timer.addEventListener(TimerEvent.TIMER,onTimer);				
			new GTween(flvPlayer,.2,{volume:volume},{ease:Sine.easeIn});
			_timer.start();						
		}		
		
		private function onTimer(...List):void
		{
			if (gState == PLAY && !seekLock)  // move the playhead
			{
				var newWidth:int = seekCur.range*flvPlayer.playheadPercentage*.01;
				new GTween(seekCur,timerInterval/1000,{width:newWidth},
									{ease:Sine.easeIn});
																
				newWidth = null;
				if ((flvPlayer.playheadPercentage > 0) || (flvPlayer.playheadPercentage <100))
					{
						var seekvol = flvPlayer.playheadPercentage;
						trace(flvPlayer.playheadPercentage);
				
						progr.width = seekvol*4.4;
					}
				
			}

			_fadeDuration ++;
			if (gState == PLAY && !faded && _fadeDuration > 10 && !screenLock) screenFadeOut();
		}
		
		private function screenFadeOut():void
		{
			if (debug) trace("FLVPlayerG.screenFadeOut.");
			if (!faded  && !screenLock)
			{	
				faded = true;
				fadeOut(screen,.8);
			}
		}		
		
		private function screenFadeIn():void
		{
			if (debug) trace("FLVPlayerG.screenFadeIn.");
			if (faded)
			{
				faded = false;
				fadeIn(screen,1,.8);				
			}
			_fadeDuration = 0;
		}
		
		// for fading out stuff
		public function fadeOut(object:Object,time:Number):void
		{
			if (debug) trace("FLVPlayerG.fadeOut."+object);
			new GTween(object, time, {alpha:0}, 
						{ease:Sine.easeIn});									

		}
		
		// for fading in stuff
		public function fadeIn(object:Object, to:Number, time:Number):void
		{			
			if (debug) trace("FLVPlayerG.fadeIn."+object);			
			object.visible = true;
			new GTween(object, time, {alpha:to}, 
						{ease:Sine.easeIn});				
		}		
		
		public function shareClick(e:Event):void
		{
			if (debug) trace("FLVPlayerG.shareClick.");
			gPause();
			ExternalInterface.call("openFlashWindow",SECTION,ID);
		}
		
	}
}