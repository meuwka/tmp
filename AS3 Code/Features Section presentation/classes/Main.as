﻿package {
	import flash.text.*;
	import com.greensock.*; 
    import com.greensock.easing.*;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.geom.PerspectiveProjection;

	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.URLLoaderDataFormat;
    import flash.net.navigateToURL;

	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.DisplayObject;
	import flash.display.StageScaleMode;
	import flash.display.MovieClip;
	import flash.display.Loader;
 	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;

    import flash.text.TextField;  
    import flash.text.TextFormat;  
    import flash.text.TextFormatAlign;
	import flash.text.AntiAliasType;
    import flash.filters.BitmapFilter;  
    import flash.filters.DropShadowFilter;
    import flash.filters.BitmapFilterQuality;
    import flash.display.Bitmap;
    import flash.display.BitmapData;
	
    import flash.events.*;	
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import flash.utils.*;
	import flash.external.*;
	
	import ScrollCameras;
	
	public class Main extends Sprite {
		const BOX_ID: String = "___BOX";
		const LABEL_ID: String = "___LABEL";
		const COUNT_BORDER_HOR: int = 6;
		const FLV_PLAYER_PATH: String = "flv_player_g_simple.swf";

		var _scrollBar:FullScreenScrollBar;
		var xmlPath="features-section.xml";
		// ------------------  for svn -----------------
		//var xmlPath="Flash/_assets/xml/features-section.xml";
		var myXMLLoader:URLLoader = new URLLoader();

		var my_x:int;
		var my_y:int;
		var netThickness:int;
		var images_path:String;
		var image_width:int;
		var image_height:int;
		var videos_path:String;
		var video_width:int;
		var video_height:int;
		var grids:XMLList;
		var total:int;
		var loaded: int = 0;
		//using for details
		var counters_path: String;
		var feature: XML;
		var bookmarks:XMLList;
		var solids:XMLList;
		var full_image_name: String;
		var full_video_name: String;

		var gallery_mc:MovieClip;
		var preloaders_mc:MovieClip;
		//using for details
		var composition:String;
		var details_mc:MovieClip;
		var details_white_line:MovieClip;
		var cameras_mc: MovieClip;
		var block: Block01;
		var scrollCameras: ScrollCameras;
		var details_gallery_mc: MovieClip;
		var share_this_mc: MovieClip ;
		var left_border_mc: MovieClip;
		var right_border_mc: MovieClip;
		var full_mc: MovieClip;
		var full_preloader: CircularPreloader;

		var gridLinesHor:Array=[];
		var gridLinesVer:Array=[];
		var solidSquares:Array=[];
		var imagesOfFeature:Array=[];
		var preloaders:Array=[];
		var titles:Array=[];
		var flag_share:Boolean;
		var isAvailable:Boolean;
		var jsFeature:String;
		var introWidth: Number;
		
		public function Main() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			isAvailable = true;
		}

		function recieveFromJS(msg) {
			if (!isAvailable) return;
			isAvailable = false;
			var timer:Timer = new Timer(3000, 1);
			timer.addEventListener(TimerEvent.TIMER, onChangeTick);
			timer.start();
			
			function onChangeTick(e:TimerEvent):void {
				e.target.stop();
				isAvailable = true;
			}

			if (gallery_mc != null) {
				if (contains(gallery_mc)) {
					killIntro(msg);
				}
			}
			if (details_mc != null) {
				if (contains(details_mc)) {
					removeChild(details_mc);
		            removeChild(details_white_line);
					changeStateToDetails(msg);
				}
			}
		}
		
		private function init():void {
			ExternalInterface.addCallback("sendVariable", recieveFromJS);//!important
			
			//if (stage.loaderInfo.parameters['xmlFile']!=undefined) {
			//xmlPath=String(stage.loaderInfo.parameters['xmlFile']);
			//}
			
			stage.align=StageAlign.TOP_LEFT;
			stage.scaleMode=StageScaleMode.NO_SCALE;

			myXMLLoader.load(new URLRequest(xmlPath));
			myXMLLoader.addEventListener(Event.COMPLETE, processXML);

			jsFeature = loaderInfo.parameters["jsFeature"];
		}

		private function onAddedToStage(e:Event):void {
			init();
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		function processXML(e:Event):void {
			var myXML:XML=new XML(e.target.data);

			counters_path=myXML.counterPath.@link;
			images_path=myXML.imagePath.@path;
			videos_path=myXML.videoPath.@path;
			netThickness=myXML.netThickness.@thickness;
			my_x=myXML.@XPOSITION;
			my_y=myXML.@YPOSITION;
			image_width=myXML.@WIDTH;
			image_height=myXML.@HEIGHT;
			video_width=myXML.@VIDEO_WIDTH;
			video_height=myXML.@VIDEO_HEIGHT;
			grids=myXML.grid;
			solids=myXML.solidSquares;
			total=grids.length();

			createContainer();
			callImages();
			redrawGridLines();

			myXMLLoader.removeEventListener(Event.COMPLETE, processXML);
			myXMLLoader=null;
		}


		function createContainer():void {
			gallery_mc = new MovieClip();
			gallery_mc.x=my_x;
			gallery_mc.y=my_y;
			addChild(gallery_mc);

			gallery_mc.addEventListener(MouseEvent.CLICK, imageClick);
			gallery_mc.addEventListener(MouseEvent.MOUSE_OVER, onOver);
			gallery_mc.addEventListener(MouseEvent.MOUSE_OUT, onOut);
			gallery_mc.buttonMode=true;

			preloaders_mc = new MovieClip();
			preloaders_mc.x=0;
			preloaders_mc.y=0;
			gallery_mc.addChild(preloaders_mc);

			// Arguments: Content to scroll, track color, grabber color, grabber press color, grip color, track thickness, grabber thickness, ease amount, whether grabber is "shiny"
			_scrollBar=new FullScreenScrollBar(gallery_mc,0x000000,0xe02223,0xb11112,0xffffff,24,24,4,false);
			addChild(_scrollBar);
		}

		function callImages():void {
			var padding:int=2;
			var currLoader: Loader;
			for (var i:int = 0; i< total; i++) {
				// build grid
				switch (grids[i].@type.toString()) {
					case "A" :
						// build grid A
						try {
							makeImageLoader(0, 0,grids[i].feature[0].introimage.@path,
											padding, grids[i].feature[0].@name);
								drawTitles(grids[i].feature[0], padding, 0, 3*(image_width+netThickness),4*(image_height+netThickness));
							drawSolidSquare(grids[i], padding, 3, 4);
							padding+=3*(image_width+netThickness);
							break;
						} catch (err: Error) {
							trace("Error: Grid incorrect");
						}
					case "B" :
						// build grid B
						try {
							makeImageLoader(0, 0, grids[i].feature[0].introimage.@path,
											padding, grids[i].feature[0].@name);
								drawTitles(grids[i].feature[0], padding,0, 3*(image_width+netThickness),2*(image_height+netThickness));
							makeImageLoader(0, 2*(image_height+netThickness), grids[i].feature[1].introimage.@path,
											padding, grids[i].feature[1].@name);
								drawTitles(grids[i].feature[1], padding, 2*(image_height+netThickness), 3*(image_width+netThickness),2*(image_height+netThickness));
							drawSolidSquare(grids[i], padding, 3, 4);
							padding+=3*(image_width+netThickness);
						} catch (err: Error) {
							trace("Error: Grid incorrect");
						}
						break;
					case "C" :
						// build grid C
						try {
							makeImageLoader(0, 0, grids[i].feature[0].introimage.@path,
											 padding, grids[i].feature[0].@name);
								drawTitles(grids[i].feature[0], padding,0, 3*(image_width+netThickness),2*(image_height+netThickness));
							makeImageLoader(0, 2*(image_height+netThickness), grids[i].feature[1].introimage.@path,
											 padding, grids[i].feature[1].@name,											 
											 makeMask(netThickness+image_width,0,image_width,image_height,
													  padding,2*image_height+3*netThickness,300,252));
							
								drawTitles(grids[i].feature[1], padding, 3*(image_height+netThickness), 2*(image_width+netThickness),1*(image_height+netThickness));
							makeImageLoader((image_width+netThickness), 2*(image_height+netThickness), grids[i].feature[2].introimage.@path,
											 padding, grids[i].feature[2].@name,									 
											 makeMask(1* netThickness, netThickness+image_height,image_width,image_height,
													  padding+image_width,2* image_height+3* netThickness,300,252));							
								drawTitles(grids[i].feature[2], padding+1*(image_width+netThickness), 2*(image_height+netThickness), 2*(image_width+netThickness),2*(image_height+netThickness));
							drawSolidSquare(grids[i], padding, 3, 4);
							padding+=3*(image_width+netThickness);
						} catch (err: Error) {
							trace("Error: Grid incorrect");
						}
						break;
					case "D" :
						// build grid D
						try {
							makeImageLoader(0, 0, grids[i].feature[0].introimage.@path,
											padding, grids[i].feature[0].@name);
								drawTitles(grids[i].feature[0], padding,0, 2*(image_width+netThickness),2*(image_height+netThickness));
							makeImageLoader(2*(image_width+netThickness), 0, grids[i].feature[1].introimage.@path,
											padding, grids[i].feature[1].@name);
								drawTitles(grids[i].feature[1], padding + 2*(image_width+netThickness),0, 3*(image_width+netThickness),2*(image_height+netThickness));
							makeImageLoader(0, 2*(image_height+netThickness), grids[i].feature[2].introimage.@path,
											padding, grids[i].feature[2].@name);
								drawTitles(grids[i].feature[2], padding, 2*(image_height+netThickness), 3*(image_width+netThickness),2*(image_height+netThickness));
							makeImageLoader(3*(image_width+netThickness), 2*(image_height+netThickness), grids[i].feature[3].introimage.@path,
											padding, grids[i].feature[3].@name);
								drawTitles(grids[i].feature[3], padding + 3*(image_width+netThickness), 2*(image_height+netThickness), 2*(image_width+netThickness),2*(image_height+netThickness));
							drawSolidSquare(grids[i], padding, 5, 4);
							padding+=5*(image_width+netThickness);
						} catch (err: Error) {
							trace("Error: Grid incorrect");
						}
						break;
				}
				if (stage.contains(_scrollBar))	_scrollBar.adjustSize();
			}
introWidth = padding;
trace("introWidth  = "+introWidth);
		}

		function drawTitles(featureXml: XML, padding_x: int, padding_y: int, imageWidth: int, imageHeight: int) {
				titles[titles.length] = [CreateRedTitle(featureXml.redtitle.@text, int(featureXml.redtitle.@x)+padding_x, int(featureXml.redtitle.@y)+padding_y+netThickness), int(featureXml.redtitle.@y)+padding_y+netThickness];
				titles[titles.length] = [CreateTitle(featureXml.title.@text, int(featureXml.title.@x)+padding_x,int(featureXml.title.@y)+padding_y+netThickness), int(featureXml.title.@y)+padding_y+netThickness];
				titles[titles.length] = [CreateSlogan(featureXml.slogan.@text,int(featureXml.slogan.@x)+padding_x,int(featureXml.slogan.@y)+padding_y+netThickness), int(featureXml.slogan.@y)+padding_y+netThickness];

				var columns = Math.floor(imageWidth / (image_width + netThickness));
				var rows = Math.floor(imageHeight / (image_height + netThickness));
				for (var i:int = 0; i<columns; i++) {
					for (var j:int = 0; j<rows; j++) {
						makePreloader(padding_x + i*(image_width+netThickness), padding_y + j*(image_height+netThickness));
					}
				}
				// specially for mask (grid C)
				if (imageHeight == 1*(image_height+netThickness)) {
					makePreloader(padding_x, padding_y - 1*(image_height+netThickness));
				}
		}

		function redrawTitles() {
			for (var i: int = 0; i < titles.length; i++) {
				if (gallery_mc.contains(titles[i][0])) {
					gallery_mc.setChildIndex(titles[i][0], gallery_mc.numChildren - 1);
				}
			}
		}
		
		function drawSolidSquare(gridXml: XML, padding: int, maxWidth: int, maxHeight) {
			for (var i:int = 0; i<gridXml.solidsquare.length(); i++) {
				if (gridXml.solidsquare[i].@x >= 0 && gridXml.solidsquare[i].@x < maxWidth
				&& gridXml.solidsquare[i].@y >= 0 && gridXml.solidsquare[i].@y < maxHeight)
					solidSquares[solidSquares.length] = makeImageLoader(gridXml.solidsquare[i].@x * (image_width+netThickness),
										gridXml.solidsquare[i].@y * (image_height+netThickness),
										gridXml.solidsquare[i].@path,
										padding, BOX_ID+i);
			}
		}

		function redrawSolidSquares() {
			for (var i: int = 0; i < solidSquares.length; i++) {
				if (gallery_mc.contains(solidSquares[i])) {
					gallery_mc.setChildIndex(solidSquares[i], gallery_mc.numChildren - 1);
				} else {
					gallery_mc.addChild(solidSquares[i]);
					TweenMax.from(solidSquares[i], 0.5, {alpha:0});
				}
			}
		}

		// universal masking for gallery_mc container
		function makeMask(mask_x:int, mask_y:int, mask_w: int, mask_h: int, image_x: int, image_y: int, image_w: int, image_h: int): MovieClip {
			var newMask: MovieClip = new MovieClip();
			newMask.graphics.beginFill(0xFF0000, 1);
			/* top left*/		newMask.graphics.drawRect(image_x, image_y,
														  mask_x, mask_y);
			/* top center*/		newMask.graphics.drawRect(image_x+mask_x, image_y,
														  mask_w, mask_y);
			/* top right*/		newMask.graphics.drawRect(image_x+mask_x+mask_w, image_y,
														  image_w-mask_x-mask_w, mask_y);
			/* center left*/	newMask.graphics.drawRect(image_x, image_y+mask_y,
														  mask_x, mask_y+mask_h);
			/* center right*/	newMask.graphics.drawRect(image_x+mask_x+mask_w, image_y+mask_y,
														  image_w-mask_x-mask_w, mask_y+mask_h);
			/* bottom left*/	newMask.graphics.drawRect(image_x, image_y+mask_y+mask_h,
														  mask_x, image_h-mask_y-mask_h);
			/* bottom center*/	newMask.graphics.drawRect(image_x+mask_x, image_y+mask_y+mask_h,
														  mask_w, image_h-mask_y-mask_h);
			/* bottom right*/	newMask.graphics.drawRect(image_x+mask_x+mask_w, image_y+mask_y+mask_h,
														  image_w-mask_x-mask_w, image_h-mask_y-mask_h);
			newMask.graphics.endFill();
			return newMask;
		}
		
		// image loader for images on intro view
		function makeImageLoader(image_x: int, image_y: int,
								 imagePath: String, padding: int, featureName: String = null,
								 imageMask: MovieClip = null):Loader {
			var imageLoader = new Loader();
			var endPath:String=images_path+imagePath;
			imageLoader.load(new URLRequest(endPath));
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
			if (featureName != null) {
				imageLoader.name = featureName;
			}
			imageLoader.x=image_x+padding;
			imageLoader.y=image_y+netThickness;
			if (imageMask) imageLoader.mask = imageMask;
			return imageLoader;
		}

		// make preloader for intro view
		function makePreloader(preloader_x: int, preloader_y: int) {
			//trace("preloader: x="+Math.floor(preloader_x/image_width)+" y="+Math.floor(preloader_y/image_height));
			var emptyBlock = new MovieClip();
			emptyBlock.x = preloader_x;
			emptyBlock.y = preloader_y;

			var emptyBitmap:Bitmap = new Bitmap();
			emptyBitmap.bitmapData = new empty(image_width, image_height);
			emptyBlock.addChild(emptyBitmap);
			emptyBitmap.width = image_width;
			emptyBitmap.x = 0;
			emptyBitmap.y = 0;
			
			emptyBlock.width=image_width+2;
			emptyBlock.height=image_height+2;

			var preloader:CircularPreloader = new CircularPreloader();
			preloader.width=24;
			preloader.height=24;
			preloader.x= image_width/ 2;
			preloader.y= image_height/ 2;
			emptyBlock.addChild(preloader);
								
			preloaders_mc.addChild(emptyBlock);
		}

		// checking intro view after image loaded
		function imageLoaded(e:Event):void {
			var currentImage:Loader=Loader(e.target.loader);
			if (currentImage.name.substr(0,6)==BOX_ID) return;
			var imageName: String = currentImage.contentLoaderInfo.url.split("/").reverse()[0];
			for (var n: int = 0; n < grids.feature.length(); n++) {
				if (grids.feature[n].introimage.@path == imageName) {
					imagesOfFeature[n] = currentImage;
					allFeaturesLoaded(n);
					break;
				}
			currentImage.contentLoaderInfo.removeEventListener(Event.COMPLETE, imageLoaded);
			}
            //redrawGridLines();
			loaded += 1;
			if (loaded == grids.feature.length()) {
				trace("all loaded");
			// Set grid spacing and line style
var numHorzLines = Math.floor(gallery_mc.height/(image_height + netThickness));
var numVertLines = Math.floor(gallery_mc.width/(image_width + netThickness)) +1;
// remove old lines
var i:int;
for (i=0; i<gridLinesHor.length; i++) {
	if (gallery_mc.contains(gridLinesHor[i])) {
		gallery_mc.removeChild(gridLinesHor[i]);
	}
}
for (i=0; i<gridLinesVer.length; i++) {
	if (gallery_mc.contains(gridLinesVer[i])) {
		gallery_mc.removeChild(gridLinesVer[i]);
	}
}				
gallery_mc.width = introWidth;
trace("introWidth  = "+introWidth);
redrawGridLines();
				gallery_mc.removeChild(preloaders_mc);
				//redrawSolidSquares();
				if (stage.contains(_scrollBar))	_scrollBar.adjustSize();
				
				redrawTitles();
				
				trace("flash var: value="+jsFeature);
				if (jsFeature != null) {
					killIntro(jsFeature);
					jsFeature = null;
				}
			}
		}
		
		function setSideVisibility(card:MovieClip, cardBack:MovieClip):void{
			if(card.rotationY >= 90){
				cardBack.visible = true;
			}
		}
		
		function allFeaturesLoaded(f: int) {
			var currentImage:Loader=imagesOfFeature[f];
			if (currentImage.mask !=null) gallery_mc.addChild(currentImage.mask);
			currentImage.alpha = 0;
			gallery_mc.addChild(currentImage);
			var imageBoxes:MovieClip = new MovieClip();
			imageBoxes.x = currentImage.x;
			imageBoxes.y = currentImage.y;
			gallery_mc.addChild(imageBoxes);

			var cloned:BitmapData = imagesOfFeature[f].content.bitmapData.clone();
			var columns:Number = Math.floor(currentImage.width/image_width);
			var rows:Number = Math.floor(currentImage.height/image_height);
			
			for (var i = 0; i < columns; i++) {
				for (var j = 0; j < rows; j++) {
					
					var currPreloader = preloaders_mc.getChildAt(0);
					// remove preloaders
					for (var pr: int = 0; pr < preloaders_mc.numChildren; pr++) {
						currPreloader = preloaders_mc.getChildAt(pr);
						if (currPreloader.x + 10 >= currentImage.x + i * (image_width + 2) && currPreloader.x < currentImage.x  + (i + 1) * (image_width + 2)
							&&
							currPreloader.y + 10 >= currentImage.y + j * (image_height + 2) && currPreloader.y < currentImage.y + (j + 1) * (image_height + 2)) {
							//trace("del preloader: x="+Math.floor(currPreloader.x/image_width)+" y="+Math.floor(currPreloader.y/image_height));
							currPreloader.visible = false;
							break;
						}
					}
					
					var card:MovieClip = new MovieClip();

					var cfb:Bitmap = new Bitmap();
					cfb.bitmapData = new empty(image_width, image_height);
					cfb.width = image_width;
					cfb.x = (cfb.width/2)*(-1);
					cfb.y = (cfb.height/2)*(-1);
					
					var cardFront:MovieClip = new MovieClip();
					cardFront.addChild(cfb);
					card.addChild(cardFront);
					
					var cbb:Bitmap = new Bitmap();
					cbb.bitmapData =  new BitmapData(image_width, image_height);
					cbb.bitmapData.copyPixels(cloned, new Rectangle(i * (image_width + 2), j * (image_height + 2), 	image_width, image_height), new Point(0,0));
					cbb.scaleX = -1;
					cbb.x = (cbb.width/2);
					cbb.y = (cbb.height/2)*(-1);
					
					var cardBack:MovieClip = new MovieClip();
					cardBack.addChild(cbb);
					cardBack.visible = false;
					card.addChild(cardBack);
					
					imageBoxes.addChild(card);
					card.x = i * (image_width + 2) + image_width/2;
					card.y = j * (image_height + 2) + image_height/2;
					
					var pp1:PerspectiveProjection = new PerspectiveProjection();
                    pp1.fieldOfView=100;
                    pp1.projectionCenter=new Point(card.x,card.y);
					card.transform.perspectiveProjection=pp1;

					TweenMax.to(card, 0.8, {delay:(i * 0.55 + j * 0.35), rotationY:180, onUpdate:setSideVisibility, ease:Quad.easeOut, onUpdateParams:[card, cardBack]});					
				}
			}
			redrawGridLines();
			TweenMax.to(currentImage, 0, {alpha:1, delay:((columns-1)*0.55+(rows-1)*0.35)+0.8, onComplete: function(){
				gallery_mc.removeChild(imageBoxes);
				redrawSolidSquares();
				//animate titles
				gallery_mc.addChild(titles[3*f][0]);
				TweenMax.from(titles[3*f][0], 0.4, {blurFilter:{blurX:20}, y:"70", alpha:0, ease:Quad.easeOut});
				gallery_mc.addChild(titles[3*f+1][0]);
				TweenMax.from(titles[3*f+1][0], 0.4, {blurFilter:{blurX:20}, y:"70", alpha:0, delay:0.2, ease:Quad.easeOut});
				gallery_mc.addChild(titles[3*f+2][0]);
				TweenMax.from(titles[3*f+2][0], 0.4, {blurFilter:{blurX:20}, y:"70", alpha:0, delay:0.4, ease:Quad.easeOut});
			}});
			if (stage.contains(_scrollBar))
				_scrollBar.adjustSize();
			redrawTitles();
			
			//currentImage.contentLoaderInfo.removeEventListener(Event.COMPLETE, imageLoaded);
		}

			
		// redraw grid lines on intro view
		function redrawGridLines():void {
			// Set grid spacing and line style
			var numHorzLines = Math.floor(gallery_mc.height/(image_height + netThickness));
			var numVertLines = Math.floor(gallery_mc.width/(image_width + netThickness)) +1;
			// remove old lines
			var i:int;
			for (i=0; i<gridLinesHor.length; i++) {
				if (gallery_mc.contains(gridLinesHor[i])) {
					gallery_mc.removeChild(gridLinesHor[i]);
				}
			}
			for (i=0; i<gridLinesVer.length; i++) {
				if (gallery_mc.contains(gridLinesVer[i])) {
					gallery_mc.removeChild(gridLinesVer[i]);
				}
			}
			// add new hor and vert lines
			var last_x: int = drawLines(numVertLines, 0x000000, 0.62, false, gallery_mc.width);
			drawLines(numHorzLines, 0x000000, 0.62, true, last_x);
		}

		// draw grid lines
		function drawLines(countLines: int, color: uint, alfa: Number, isHorizontal: Boolean, last_x: int):int {
			var i:int;
			var gridLine:Shape;
			var hl:Graphics;
			for (i = 0; i < countLines; i++) {
				gridLine = new Shape();
				gridLine.graphics.lineStyle(netThickness, color, alfa, false);
				gridLine.graphics.moveTo(0,0);
				if (isHorizontal) {
					gridLine.graphics.lineTo(last_x,0);
					gridLinesHor[i]=gridLine;
					gridLine.x = 0;
					gridLine.y = i * (image_height+netThickness) + netThickness/2;
				} else {
					gridLine.graphics.lineTo(0,gallery_mc.height);
					gridLinesVer[i]=gridLine;
					gridLine.x = i * (image_width+netThickness) + netThickness/2;
					gridLine.y = 0;
				}
				gallery_mc.addChild(gridLine);
			}
			return gridLinesVer.length > 0 ? gridLinesVer[i-1].x : 0;
		}
		
		// change state to details from intro
		function imageClick(e:MouseEvent):void {
			// remove intro
			if (e.target.name.substr(0,6)==BOX_ID) return;

			if (e.target.name.substr(0,8)==LABEL_ID) {
				var i: int;
				for (i=0; i<titles.length; i++) {
					if (titles[i][0] == e.target) {
						killIntro(grids.feature[(int)(i / 3)].@name);
						return;
					}
				}
				//killIntro(grids.feature[(int)(titles.indexOf(e.target) / 3)].@name);
			} else {
				killIntro(e.target.name);
			}
		}
		
		// draw details border (default: red/grey boxes
		function drawBorder(parent_mc: MovieClip, count_hor: int, solid_path: String) {
			var count_vert: int = 4;
			var i: int;
			var j: int;
			for (i=0; i<count_vert; i++) {
				for (j=0; j<count_hor; j++) {
					var imageLoader:Loader = new Loader();
					imageLoader.load(new URLRequest(solid_path));
					imageLoader.x=j*(image_width+netThickness);
					imageLoader.y=i*(image_height+netThickness)+netThickness;
					parent_mc.addChild(imageLoader);
				}
			}
		}
		
		// draw grid for details media parts (photo/video gallery)
		function drawGalleryGrid(): MovieClip {
			var i: int = 0;
			var grid: MovieClip = new MovieClip();
			grid.graphics.lineStyle(netThickness, 0x000000, 0.62, false);
			for (i=0; i<5; i++) {
				// horizontal
				grid.graphics.moveTo(netThickness/2,i*(image_height+netThickness)+netThickness/2);
				grid.graphics.lineTo(4*(image_width+netThickness)+netThickness/2,i*(image_height+netThickness)+netThickness/2);
				// vartical
				grid.graphics.moveTo(i*(image_width+netThickness)+netThickness/2,netThickness/2);
				grid.graphics.lineTo(i*(image_width+netThickness)+1,4*(image_height+netThickness)+netThickness/2);
			}
			return grid;
		}
		
		function killIntro(featureName: String): void {
			var index:int = 0;
			for ( index=0; index<grids.feature.length(); index++) {
				if (grids.feature[index].@name == featureName) {
					break;
				}
			}
			if (index == grids.feature.length()) return;
			TweenMax.to(gallery_mc, 1, {delay:0.15, alpha:0, onComplete: function(){
					    while (gallery_mc.numChildren > 0) {
				               gallery_mc.removeChildAt(0);
			                   }
			}});
			//animate text
			
		    TweenMax.to(titles[3*index+1][0], 0.3, {blurFilter:{blurX:20}, y:"70", alpha:0, delay:0.15, ease:Quad.easeOut});
		    TweenMax.to(titles[3*index+2][0], 0.3, {blurFilter:{blurX:20}, y:"70", alpha:0, ease:Quad.easeOut});
			TweenMax.to(titles[3*index][0], 0.3, {blurFilter:{blurX:20}, y:"70", alpha:0, delay:0.3, ease:Quad.easeOut,
										 onComplete: function(){

				TweenMax.to(titles[3*index][0], 0, {blurFilter:{blurX:0}, y:titles[3*index][1], alpha:1});
				TweenMax.to(titles[3*index+1][0], 0, {blurFilter:{blurX:0}, y:titles[3*index+1][1], alpha:1});
				TweenMax.to(titles[3*index+2][0], 0, {blurFilter:{blurX:0}, y:titles[3*index+2][1], alpha:1,
												 onComplete: function() {
													removeChild(gallery_mc);
													_scrollBar.kill();
					                                removeChild(_scrollBar);
					                                changeStateToDetails(featureName);
												 }});			     
					 }});
		}

		// implementation changing view (to details)
		function changeStateToDetails(featureName: String): void {
			// prepare screen
			stage.align=StageAlign.TOP;
			stage.scaleMode=StageScaleMode.NO_SCALE;
			// add details view
			details_mc = new MovieClip();
			details_mc.graphics.beginFill(0x1e1e1e);
			details_mc.graphics.drawRect(0, 0, 1056, 506);
			details_mc.graphics.endFill();
			details_mc.x = -27; // magic bug: make left padding = -127;
			details_mc.y = 0;
			addChild(details_mc);
			TweenMax.from(details_mc, 0.9, {alpha:0});
			
			//make background/border
			left_border_mc = new MovieClip();
			left_border_mc.x = 0 - COUNT_BORDER_HOR*(image_width+netThickness); //block.x
			drawBorder(left_border_mc, COUNT_BORDER_HOR, images_path+solids.leftBorder.@path);
			details_mc.addChild(left_border_mc);
			right_border_mc = new MovieClip();
			right_border_mc.x = 1054+netThickness; //details_gallery_mc.width+details_gallery_mc.x+netThickness
			drawBorder(right_border_mc, COUNT_BORDER_HOR,images_path+solids.rightBorder.@path);
			details_mc.addChild(right_border_mc);
			
			details_white_line = new MovieClip();
            details_white_line.graphics.beginFill(0xFFFFFF);
            details_white_line.graphics.drawRect(details_mc.x-COUNT_BORDER_HOR*(image_width+netThickness), 506,
												details_mc.width+2*COUNT_BORDER_HOR*(image_width+netThickness), 24);
            details_white_line.graphics.endFill();
		    addChild(details_white_line);
			
			details_gallery_mc = new MovieClip();
			details_gallery_mc.x = 454; //block.width-netThickness
			details_gallery_mc.y = 0;
			details_mc.addChild(details_gallery_mc);
			// add preloader
			var preloader: CircularPreloader = new CircularPreloader();
			preloader.width = 24;
			preloader.height = 24;
			preloader.x = details_mc.width / 2;
			preloader.y = details_mc.height / 2;
			preloader.name = "detailsPreloader";
			details_mc.addChild(preloader);
			
			block = new Block01();
			// load details data: TEXT
			for (var i: int=0; i<grids.feature.length(); i++) {
				if (grids.feature[i].@name == featureName) {
					feature = grids.feature[i];
					break;
				}
			}
			
			composition = feature.details.mainimage.@composition; //read flag yes|no for main image
			
            var _font1 : Font = new Swis721CnBTItalic();
			var textFormat1:TextFormat = new TextFormat(_font1.fontName,41, 0xFF3334);
			textFormat1.bold = true;
			textFormat1.italic = true;
			
			var _font2 : Font = new Swiss721BoldCondensedBT();
			var textFormat2:TextFormat = new TextFormat(_font2.fontName,41, 0xFFFFFF);
			textFormat2.bold = true;
			textFormat2.italic = false;
			
			block.titleText.txt.embedFonts = true;
			block.titleText.txt.text = "";
			var beginIndex: int = 0;
			var redText:String = new String(feature.details.title.@redtext);
			var whiteText:String = new String(feature.details.title.@text);
			
			if (redText != ""){
				block.titleText.txt.text = redText.toUpperCase();
				block.titleText.txt.appendText(" ");
				if (redText.length + whiteText.length + 1 > 18 )
				{
					block.titleText.txt.appendText("\n");
			    }
				block.titleText.txt.setTextFormat(textFormat1);
				beginIndex = block.titleText.txt.length;
				}
			block.titleText.txt.appendText(whiteText.toUpperCase());
			var endIndex = block.titleText.txt.length;
			block.titleText.txt.setTextFormat(textFormat2, beginIndex, endIndex);

			
			if (block.titleText.txt.numLines == 2)
			{
				block.sloganText.y = block.titleText.y + block.titleText.txt.textHeight + 2;
			}
			else
			{
				block.sloganText.y = block.titleText.y + block.titleText.txt.textHeight + 4;
			}
			block.sloganText.txt.text = feature.details.slogan.@text;
			
			//description text
			var scrollbar:ScrollBar;
			
			block.descriptionText.y = block.sloganText.y + block.sloganText.txt.textHeight + 10;
			block.descriptionText.txt.condenseWhite = true;
			block.descriptionText.txt.htmlText = feature.details.description.@text;
			
			if ((block.titleText.txt.numLines == 2)&&(block.descriptionText.txt.numLines > 4))
			{
    			if ((block.sloganText.txt.numLines == 2)&&(block.descriptionText.txt.numLines > 3))
			    {
				    block.descriptionText.txt.height = 60;
					scrollbar = new ScrollBar(block.descriptionText.txt);
                    block.descriptionText.addChild(scrollbar);
			    }
			    else
				{
					block.descriptionText.txt.height = 80;
					scrollbar = new ScrollBar(block.descriptionText.txt);
                    block.descriptionText.addChild(scrollbar);
				}
			}
			if ((block.titleText.txt.numLines == 2)&&(block.descriptionText.txt.numLines <= 4))
			{
				block.descriptionText.txt.height = block.descriptionText.txt.numLines * 20;
			}
			if ((block.titleText.txt.numLines == 1)&&(block.descriptionText.txt.numLines > 6))
			{
				block.descriptionText.txt.height = 112;
				scrollbar = new ScrollBar(block.descriptionText.txt);
                block.descriptionText.addChild(scrollbar);
			}
			if ((block.titleText.txt.numLines == 1)&&(block.descriptionText.txt.numLines <= 6))
			{
				block.descriptionText.txt.height = block.descriptionText.txt.numLines * 20;
			}

            // configure button
			if (feature.details.menu.photos.picture.length() > 0) {
				block.viewgallery.btnPhotos.buttonMode = true;
				block.viewgallery.btnPhotos.addEventListener(MouseEvent.CLICK, btnPhotosClick);
				block.viewgallery.btnPhotos.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			        block.viewgallery.btnPhotos.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			} else {
				block.viewgallery.btnPhotos.visible = false;
			}
			if (feature.details.menu.videos.video.length() > 0) {
				block.viewgallery.btnVideos.buttonMode = true;
				block.viewgallery.btnVideos.addEventListener(MouseEvent.CLICK, btnVideosClick);
				block.viewgallery.btnVideos.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			    block.viewgallery.btnVideos.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			} else {
				block.viewgallery.btnVideos.visible = false;
			}
			
			if (feature.details.menu.socialShares.share.length() > 0)
			{
				block.viewgallery.btnShareThis.buttonMode = true;
				block.viewgallery.btnShareThis.addEventListener(MouseEvent.CLICK, btnShareThisClick);
				block.viewgallery.btnShareThis.addEventListener(MouseEvent.MOUSE_OVER, redOver);
				block.viewgallery.btnShareThis.addEventListener(MouseEvent.MOUSE_OUT, shareOut);
				bookmarks=feature.details.menu.socialShares.share;
			} else {
				block.viewgallery.btnShareThis.visible = false;
			}
			
			block.viewgallery.btnLike.buttonMode = true;
			block.viewgallery.btnLike.addEventListener(MouseEvent.CLICK, btnLikeClick);
			block.viewgallery.btnLike.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			block.viewgallery.btnLike.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			block.viewgallery.btnLike_disable.visible = false;
            block.viewgallery.txtLike.txt.text = "";
            block.btn_ViewAll.buttonMode = true;
			block.btn_ViewAll.addEventListener(MouseEvent.CLICK, btnViewAllClick);
			block.btn_ViewAll.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			block.btn_ViewAll.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			
            // build cameras gallery
			cameras_mc = new MovieClip();
			cameras_mc.x = block.x;
			cameras_mc.y = 380;
			scrollCameras = new ScrollCameras(images_path);
			scrollCameras.addEventListener("AllLoaded", camerasLoaded);
			scrollCameras.create(cameras_mc,feature);
			
			stage.addEventListener(KeyboardEvent.KEY_UP, keyPressed);
			buildRequest("AddCounter","view",onUrlCompleteGetAll);
			buildRequest("getcounerdetails","like",onUrlCompleteGetLike);
		}
		
		// back action by pressed backspace
		function keyPressed(e: KeyboardEvent):void {
			if (Keyboard.BACKSPACE == e.keyCode) {
				backClick(new MouseEvent("Click"));
			}
		}
		
		// all cameras loaded - other part load (details view)
		function camerasLoaded(e: Event):void {
			  //trace("all cameras loaded");
			  e.target.removeEventListener("AllLoaded", camerasLoaded);
			  var imageLoader:Loader = new Loader();
			  imageLoader.load(new URLRequest(images_path+feature.details.mainimage.@path));
			  imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaded);
			  imageLoader.alpha = 0;
			  imageLoader.x = 2;
			  imageLoader.y = 2;
			  imageLoader.name = "photoContent";
			  
			  details_gallery_mc.addChild(imageLoader);
		   }
		
		function onLoaded(e:Event):void	{
            e.target.removeEventListener(Event.COMPLETE, onLoaded);
			createBlock01Animation(block);
		}

		// create mask for labels
		function createMask(mask_x:int, mask_y:int, mask_w:int, mask_h:int): MovieClip {
			var newMask: MovieClip = new MovieClip();
			newMask.graphics.beginFill(0xFF0000, 1);
			newMask.graphics.drawRect(mask_x, mask_y, mask_w, mask_h);
			newMask.graphics.endFill();
			return newMask;
		}
		
		function createBlock01Animation(block:MovieClip):void{
			var maskViewGallery = createMask(block.viewgallery.x, block.viewgallery.y, block.viewgallery.width, block.viewgallery.height);
		    block.viewgallery.mask = maskViewGallery;
			var maskTitleText = createMask(block.titleText.x - block.titleText.width, block.titleText.y, block.titleText.width +3, block.titleText.height);
			block.titleText.mask = maskTitleText;
			var maskSloganText = createMask(block.sloganText.x, block.sloganText.y - block.sloganText.height, block.sloganText.width, block.sloganText.height);
			block.sloganText.mask = maskSloganText;
			var maskDescrText = createMask(block.descriptionText.x, block.descriptionText.y - block.descriptionText.height, block.descriptionText.width, block.descriptionText.height);
			block.descriptionText.mask = maskDescrText;
			block.addChild(maskTitleText);
			block.addChild(maskSloganText);
			block.addChild(maskDescrText);
			block.addChild(maskViewGallery);
			
			var cameras_bg_mc: MovieClip = new MovieClip();
			cameras_bg_mc.x = block.x;
			cameras_bg_mc.y = 380;
			createBg(cameras_bg_mc);
			
		    var preloader = details_mc.getChildByName("detailsPreloader");
			TweenMax.to(preloader, 0.5, {alpha:0});
			details_mc.addChild(cameras_bg_mc);
			TweenMax.from(cameras_bg_mc, 0.5, {alpha:0});
			details_mc.addChild(block); // add Block01
			TweenMax.from(block, 0.5, {alpha:0});
			TweenMax.to(maskTitleText, 0.45, {x:block.titleText.width-3, ease:Sine.easeIn,onComplete: function(){
						details_mc.removeChild(preloader);
						createBlock03Animation(details_gallery_mc);
						TweenMax.to(maskSloganText, 0.35, {y:block.sloganText.height, ease:Sine.easeIn, onComplete: function(){
							  TweenMax.to(maskDescrText, 0.35, {y:block.descriptionText.height, ease:Sine.easeIn, onComplete: function(){
						              TweenMax.from(block.viewgallery, 0.75, {y:block.viewgallery.y + block.viewgallery.height, ease:Quad.easeIn});
									  TweenMax.to(block.viewgallery, 0.55, {delay:0.20, alpha:1, overwrite:false, ease:Sine.easeIn, onComplete: function(){
                                         TweenMax.to(block.btn_ViewAll, 0.65, {alpha:1,ease:Sine.easeIn});
										 TweenMax.to(block.title_Cameras, 0.65, {alpha:1,ease:Sine.easeIn});
										 details_mc.addChild(cameras_mc);
									     TweenMax.from(cameras_mc, 0.65, {alpha:0, ease:Sine.easeIn, onComplete: function(){
										 TweenMax.to(block.back, 0.65, {x:12.5, y:8.8, alpha:1, ease:Sine.easeIn}); //animation for button Back
			                             block.back.buttonMode = true;
			                             block.back.addEventListener(MouseEvent.CLICK, backClick);
				                         block.back.addEventListener(MouseEvent.MOUSE_OVER, redOver);
										 block.back.addEventListener(MouseEvent.MOUSE_OUT, redOut);
										 scrollCameras.simulateScroll();
		                                  }});
									  }});
						      }});
						}});
			}});
			
		}

		function createBg(parent_mc: MovieClip):void {
			for (var i:int=0; i<3; i++)	{
				var item: block_empty = new block_empty();
				item.width = 150;
				item.x = i * 152 ;
			    item.y = 0;
				parent_mc.addChild(item);
			}			
		}
			
		function redOut(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(1);
		}
		
		function redOver(event:MouseEvent):void {
			MovieClip(event.currentTarget).gotoAndStop(2);
		}
			
		function createBlock03Animation(block03:MovieClip):void{
			
			var currentImage = block03.getChildByName("photoContent");
			var maskImage:MovieClip = new MovieClip;
			maskImage = createMask(currentImage.x, currentImage.y, image_width * 4 + 6, image_height * 4 + 6 );
		   
			
			var imageBoxes:MovieClip = new MovieClip();
			imageBoxes.x = currentImage.x;
			imageBoxes.y = currentImage.y;
			block03.addChild(imageBoxes);
			if (composition == "no") {block03.addChild(drawGalleryGrid());}
			var cloned:BitmapData = currentImage.content.bitmapData.clone();
			var columns:Number = Math.floor(currentImage.width/image_width);
			var rows:Number = Math.floor(currentImage.height/image_height);

			for (var i = 0; i < columns; i++) {
				for (var j = 0; j < rows; j++) {
					var card:MovieClip = new MovieClip();

					var cfb:Bitmap = new Bitmap();
					cfb.bitmapData = new empty(image_width, image_height);
					cfb.width = image_width;
					cfb.x = (cfb.width/2)*(-1);
					cfb.y = (cfb.height/2)*(-1);
					
					var cardFront:MovieClip = new MovieClip();
					cardFront.addChild(cfb);
					card.addChild(cardFront);
					var cbb:Bitmap = new Bitmap();
					cbb.bitmapData =  new BitmapData(image_width + 2, image_height + 2);
					cbb.bitmapData.copyPixels(cloned, new Rectangle(i * (image_width + 2), j * (image_height + 2), image_width + 2, image_height + 2), new Point(0,0));
					cbb.scaleX = -1;
					cbb.x = (cbb.width/2);
					cbb.y = (cbb.height/2)*(-1);
					
					var cardBack:MovieClip = new MovieClip();
					cardBack.addChild(cbb);
					cardBack.visible = false;
					card.addChild(cardBack);
					
					imageBoxes.addChild(card);
					card.x = i * (image_width + 2) + image_width/2;
					card.y = j * (image_height + 2) + image_height/2;
					
					var pp2:PerspectiveProjection = new PerspectiveProjection();
                    pp2.fieldOfView = 100;
                    pp2.projectionCenter = new Point(card.x,card.y);
					card.transform.perspectiveProjection = pp2;

					TweenMax.to(card, 0.6, {delay:(i * 0.35 + j * 0.15), rotationY:180, onUpdate:setSideVisibility, ease:Quad.easeOut, onUpdateParams:[card, cardBack]});
				}
			}
			TweenMax.to(currentImage, 0, {alpha:1, delay:((columns-1)*0.35+(rows-1)*0.15)+0.6, onComplete: function(){
				currentImage.mask = maskImage;
				block03.addChild(maskImage);
				block03.removeChild(imageBoxes);
		    }});
		}

		function btnViewAllClick(e:MouseEvent):void {
			trace("viewall="+feature.details.allcameras.@path);
			navigateToURL(new URLRequest(feature.details.allcameras.@path), "_blank");
		}

		       
       function btnPhotosClick(e:MouseEvent):void {
			while (details_gallery_mc.numChildren > 0) {
				details_gallery_mc.removeChildAt(0);
			}
			var i: int;
			var images_count=feature.details.menu.photos.picture.length();
			preloaders = new Array();
			for (i=0; i<16; i++) {
				var empty: block_empty = new block_empty();
				empty.x = (i%4)*(image_width+netThickness)+netThickness;
				empty.y = Math.floor(i/4)*(image_height+netThickness)+netThickness;
				details_gallery_mc.addChild(empty);
				TweenMax.from(empty, 0.5, {alpha:0, ease:Sine.easeIn});
			}
			for (i=0; i<images_count; i++) {
				if (i == 16) break;
				var imageContainer:MovieClip = new MovieClip();
				var imageLoader:Loader = new Loader();
				imageLoader.load(new URLRequest(images_path+feature.details.menu.photos.picture[i].@thumb));
				imageLoader.name = feature.details.menu.photos.picture[i].@path;
				details_gallery_mc.addChild(imageContainer);
				imageContainer.buttonMode = true;
				imageContainer.x = (i%4)*(image_width+netThickness)+netThickness;
				imageContainer.y = Math.floor(i/4)*(image_height+netThickness)+netThickness;
				imageContainer.addEventListener(MouseEvent.CLICK, imageFullClick);
				imageContainer.addEventListener(MouseEvent.MOUSE_OUT, photoOut);
				imageContainer.addEventListener(MouseEvent.MOUSE_OVER, photoOver);
				imageContainer.addChild(imageLoader);
				TweenMax.from(imageLoader,1.5, { alpha:0});
				// make preloaders
				var preloader:CircularPreloader = new CircularPreloader();
				preloader.width=24;
				preloader.height=24;
				preloader.x=(i%4)*(image_width+netThickness)+netThickness+image_width/2;
				preloader.y= Math.floor(i/4)*(image_height+netThickness)+netThickness+image_height/2;
				details_gallery_mc.addChild(preloader);
				preloaders[preloaders.length] = preloader;
				details_gallery_mc.setChildIndex(imageContainer, details_gallery_mc.numChildren -1);
			}
			var gallery_label: galleryTitle = new galleryTitle();
			gallery_label.txt.text = "PHOTO GALLERY";
			gallery_label.x = netThickness;
			gallery_label.y = 1.5*netThickness;
			details_gallery_mc.addChild(gallery_label);
		}
		
		function photoOver(event:MouseEvent):void {
			TweenMax.from(MovieClip(event.currentTarget),1, {colorMatrixFilter:{contrast:1.7, brightness:1.7, saturation:0.5}});
		}
		function photoOut(event:MouseEvent):void {
			TweenMax.to(MovieClip(event.currentTarget),0.5, {colorMatrixFilter:{contrast:1, brightness:1, saturation:1}});
		}
		
		function imageFullClick(e:MouseEvent):void {
			imageFullLoad(e.target.name);
		}
		
		function imageFullLoad(imagePath: String): void {
			full_mc = new MovieClip();
			full_mc.graphics.beginFill(0x333333, 0.8);
			full_mc.graphics.drawRect(2, 2, details_gallery_mc.width, details_gallery_mc.height);
			full_mc.graphics.endFill();
			details_gallery_mc.addChild(full_mc);
			// loading image
			var full_loader:Loader = new Loader();
				full_image_name = imagePath;
			full_loader.load(new URLRequest(images_path+imagePath));
			full_loader.contentLoaderInfo.addEventListener(Event.INIT, fullLoaded);
			full_mc.addChild(full_loader);
			// make preloader
			full_preloader= new CircularPreloader();
			full_preloader.width=24;
			full_preloader.height=24;
			full_preloader.x=full_mc.width / 2;
			full_preloader.y=full_mc.height / 2;
			full_mc.addChild(full_preloader);
			// make button close
			var btnPhotoClose: btnClose = new btnClose();
			btnPhotoClose.x = full_mc.width - 35;
			btnPhotoClose.y = 7;
			btnPhotoClose.buttonMode = true;
			btnPhotoClose.addEventListener(MouseEvent.CLICK,removeFull);
			btnPhotoClose.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			btnPhotoClose.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			full_mc.addChild(btnPhotoClose);
			// add next/prev arrows
			var nextBtn: btn_Next = new btn_Next();
			nextBtn.x = full_mc.width - 65;
			nextBtn.y = 7;
			nextBtn.buttonMode = true;
			nextBtn.addEventListener(MouseEvent.CLICK, nextClick);
			nextBtn.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			nextBtn.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			full_mc.addChild(nextBtn);
			var prevBtn: btn_Prev = new btn_Prev();
			prevBtn.x = full_mc.width - 95;
			prevBtn.y = 7;
			prevBtn.buttonMode = true;
			prevBtn.addEventListener(MouseEvent.CLICK, prevClick);
			prevBtn.addEventListener(MouseEvent.MOUSE_OVER, redOver);
			prevBtn.addEventListener(MouseEvent.MOUSE_OUT, redOut);
			full_mc.addChild(prevBtn);
			
			full_loader.addEventListener(MouseEvent.CLICK,removeFull);
			full_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, fullLoaded);
		}
		
		function fullLoaded(e:Event):void {
			// move image center
			var full_loader:Loader = Loader(e.target.loader);
			full_loader.x = (full_mc.width - full_loader.width) / 2;
			full_loader.y = (full_mc.height - full_loader.height) / 2;
			TweenMax.from(full_loader,1, { alpha:0});
			// delete preloader
			full_mc.removeChild(full_preloader);
		}

		function nextClick(e:MouseEvent):void {
			var imagePath: String;
			var photos: XMLList = feature.details.menu.photos.picture;
			var count = photos.length();
			for (var i: int=0; i< count; i++) {
				if (full_image_name == photos[i].@path) {
					if (i != count-1) {
						imagePath = photos[i+1].@path;
					} else {
						imagePath = photos[0].@path;
					}
					break;
				}
			}
			removeFull(e);
			imageFullLoad(imagePath);
		}

		function prevClick(e:MouseEvent):void {
			var imagePath: String;
			var photos: XMLList = feature.details.menu.photos.picture;
			var count = photos.length();
			for (var i: int=0; i< count; i++) {
				if (full_image_name == photos[i].@path) {
					if (i != 0) {
						imagePath = photos[i-1].@path;
					} else {
						imagePath = photos[count-1].@path;
					}
					break;
				}
			}
			removeFull(e);
			imageFullLoad(imagePath);
		}
		
		function nextClickVideo(e:MouseEvent):void {
			var videoPath: String;
			var videos: XMLList = feature.details.menu.videos.video;
			var count = videos.length();
			for (var i: int=0; i< count; i++) {
				if (full_video_name == videos[i].@path) {
					if (i != count-1) {
						videoPath = videos[i+1].@path;
					} else {
						videoPath = videos[0].@path;
					}
					break;
				}
			}
			removeFull(e);
			videoFullLoad(videoPath);
		}
		
		function prevClickVideo(e:MouseEvent):void {
			var videoPath: String;
			var videos: XMLList = feature.details.menu.videos.video;
			var count = videos.length();
			for (var i: int=0; i< count; i++) {
				if (full_video_name == videos[i].@path) {
					if (i != 0) {
						videoPath = videos[i-1].@path;
					} else {
						videoPath = videos[count-1].@path;
					}
					break;
				}
			}
			removeFull(e);
			videoFullLoad(videoPath);
		}
		
		function removeFull(e:MouseEvent):void {
			while (full_mc.numChildren > 0) {
				full_mc.removeChildAt(0);
			}
			full_mc.removeEventListener(MouseEvent.CLICK,removeFull);
			details_gallery_mc.removeChild(full_mc);
		}

		function btnVideosClick(e:MouseEvent):void {
			while (details_gallery_mc.numChildren > 0) {
				details_gallery_mc.removeChildAt(0);
			}
			var i: int;
			var images_count=feature.details.menu.videos.video.length();
			preloaders = new Array();

			for (i=0; i<16; i++) {
				var empty: block_empty = new block_empty();
				empty.x = (i%4)*(image_width+netThickness)+netThickness;
				empty.y = Math.floor(i/4)*(image_height+netThickness)+netThickness;
				details_gallery_mc.addChild(empty);
				TweenMax.from(empty, 0.5, {alpha:0, ease:Sine.easeIn});
			}
			for (i=0; i<images_count; i++) {
				
				if (i == 16) break;
				var imageContainer:MovieClip = new MovieClip();
				var imageLoader:Loader = new Loader();
				//trace(images_path+feature.details.images.image.@thumb);
				imageLoader.load(new URLRequest(videos_path+feature.details.menu.videos.video[i].@thumb));
				imageLoader.name = feature.details.menu.videos.video[i].@path;
				details_gallery_mc.addChild(imageContainer);
				imageContainer.buttonMode = true;
				imageContainer.x = (i%4)*(image_width+netThickness)+netThickness;
				imageContainer.y = Math.floor(i/4)*(image_height+netThickness)+netThickness;
				imageContainer.addEventListener(MouseEvent.CLICK, videoFullClick);
				imageContainer.addEventListener(MouseEvent.MOUSE_OUT, photoOut);
				imageContainer.addEventListener(MouseEvent.MOUSE_OVER, photoOver);
				imageContainer.addChild(imageLoader);
				TweenMax.from(imageLoader,1.5, { alpha:0});
				// make preloaders
				var preloader:CircularPreloader = new CircularPreloader();
				preloader.width=24;
				preloader.height=24;
				preloader.x=(i%4)*(image_width+netThickness)+netThickness+image_width/2;
				preloader.y= Math.floor(i/4)*(image_height+netThickness)+netThickness+image_height/2;
				details_gallery_mc.addChild(preloader);
				preloaders[preloaders.length] = preloader;
				details_gallery_mc.setChildIndex(imageContainer, details_gallery_mc.numChildren -1);
			}
			var gallery_label: galleryTitle = new galleryTitle();
			gallery_label.txt.text = "VIDEO GALLERY";
			gallery_label.x = netThickness;
			gallery_label.y = 1.5*netThickness;
			details_gallery_mc.addChild(gallery_label);
		}

		function videoFullClick(e:MouseEvent):void {
			videoFullLoad(e.target.name);
		}

		function videoFullLoad(videoPath: String):void {
			full_mc = new MovieClip();
			full_mc.buttonMode = true;
			full_mc.graphics.beginFill(0x333333, 0.8);
			full_mc.graphics.drawRect(2, 2, details_gallery_mc.width, details_gallery_mc.height);
			full_mc.graphics.endFill();
			details_gallery_mc.addChild(full_mc);
			// loading video
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.INIT,loadVideoComplete);
			loader.x = 0;
			loader.y = 0;
			loader.name = videoPath;
			full_video_name = videoPath;
			loader.load(new URLRequest(FLV_PLAYER_PATH));					
			// make preloader
			full_preloader= new CircularPreloader();
			full_preloader.width=24;
			full_preloader.height=24;
			full_preloader.x=full_mc.width / 2
			full_preloader.y=full_mc.height / 2
			full_mc.addChild(full_preloader);
			// add next/prev arrows
			var nextBtn: btn_Next = new btn_Next();
			nextBtn.x = full_mc.width - 60;
			nextBtn.y = 7;
			nextBtn.buttonMode = true;
			nextBtn.addEventListener(MouseEvent.CLICK, nextClickVideo);
			full_mc.addChild(nextBtn);
			var prevBtn: btn_Prev = new btn_Prev();
			prevBtn.x = full_mc.width - 85;
			prevBtn.y = 7;
			prevBtn.buttonMode = true;
			prevBtn.addEventListener(MouseEvent.CLICK, prevClickVideo);
			full_mc.addChild(prevBtn);
			// make button close
			var btnVideoClose: btnClose = new btnClose();
			btnVideoClose.x = full_mc.width - 35;
			btnVideoClose.y = 7;
			btnVideoClose.addEventListener(MouseEvent.CLICK,removeFull);
			full_mc.addChild(btnVideoClose);
		}
		
		function loadVideoComplete(e:Event):void
		{				
			var movie:MovieClip = e.currentTarget.content;
			movie.autoplay = true;
			movie.gStop();
			movie.addEventListener(Event.ADDED,movieLoaded);
			//movie.setDims(video_width,video_height);
			movie.setSource(videos_path + e.target.loader.name);
			full_mc.addChild(movie);							
			movie.x = (full_mc.width - video_width) / 2;
			movie.y = (full_mc.height - video_height) / 2;
		}
		
		function movieLoaded(e:Event): void {
			var player:MovieClip = MovieClip(e.currentTarget);
			player.x = (full_mc.width - player.width) / 2;
			player.y = (full_mc.height - player.height) / 2;
		}
		
		function btnShareThisClick(e:MouseEvent):void {
			flag_share = true;
			if (share_this_mc != null) {
				if (block.contains(share_this_mc)) return;
			}
			share_this_mc = new MovieClip();
			var count_bookmarks: int = bookmarks.length();
			var count_column = Math.floor(Math.sqrt(count_bookmarks/2));
			var row_num = -1;
			var btnHeight = 30;
			for (var i:int = 0; i < count_bookmarks; i++) {
				if (i % count_column == 0) { 
					row_num++;
				}
				var newBookmark:MovieClip = new MovieClip();

				var loader: Loader = new Loader();
				loader.load(new URLRequest(images_path+bookmarks[i].@iconPath));
				loader.mouseEnabled = false;
				loader.x = 0;
				loader.y = 0;
				newBookmark.addChild(loader);
				
				var _font : Font = new Swis721BTRom();
			    var textFormat:TextFormat = new TextFormat(_font.fontName,11, 0x000000);
				var textBookmark: TextField = new TextField();
				textBookmark.embedFonts = true;
				textBookmark.text = bookmarks[i].@text;
				//textBookmark.autoSize = TextFieldAutoSize.LEFT;
				textBookmark.setTextFormat (textFormat);
				textBookmark.x = 20;
				textBookmark.y = 0;
				textBookmark.height = btnHeight;
				textBookmark.mouseEnabled = false;
				newBookmark.addChild(textBookmark);
				
				newBookmark.name = bookmarks[i].@name;
				newBookmark.addEventListener(MouseEvent.CLICK, bookmarkerClick);
				newBookmark.buttonMode = true;
				share_this_mc.addChild(newBookmark);
				newBookmark.x = (i % count_column) * 110 + 20;
				newBookmark.y = (row_num + 0.5) * btnHeight;
			}
			share_this_mc.addEventListener(MouseEvent.ROLL_OUT, outShareThis);
			share_this_mc.addEventListener(MouseEvent.ROLL_OVER, overShareThis);
			// add background
			block.addChild(share_this_mc);
			TweenMax.from(share_this_mc, 0.5, {alpha:0, ease:Sine.easeIn});
			share_this_mc.graphics.beginFill(0xFFFFFF);
			share_this_mc.graphics.drawRect(0,0,1.1*share_this_mc.width+15,(row_num+1.5)*btnHeight);
			share_this_mc.graphics.endFill();
			TweenMax.to(share_this_mc, 0.1, {dropShadowFilter:{color:0x000000, alpha:0.8, blurX:9, blurY:9, angle:25}});

			// add arrow
			var whiteArrow: block01_Share = new block01_Share();
			whiteArrow.x=share_this_mc.width / 2;
			whiteArrow.y=share_this_mc.height-1;
			share_this_mc.addChild(whiteArrow);

			share_this_mc.x = e.target.x - share_this_mc.width / 2;
			share_this_mc.y = block.viewgallery.y + e.target.y - (row_num+1.5)*btnHeight - whiteArrow.height;
		}
		
		function shareOut(event:MouseEvent):void {
			flag_share = false;
			MovieClip(event.currentTarget).gotoAndStop(1);
			if ((share_this_mc != null)&&(share_this_mc.numChildren > 0)){
			TweenLite.to(MovieClip(event.currentTarget), 0.7, {onComplete:function(){
													            if (!flag_share){
				                                                removeShareThis();
			                                                    }
													           }});
			}
			
		}
		
		function outShareThis(e:MouseEvent): void {
			flag_share = false;
			//share_this_mc.removeEventListener(MouseEvent.ROLL_OUT, outShareThis);
			TweenLite.to(share_this_mc, 0.7, {onComplete:function(){
													            if (!flag_share){
				                                                removeShareThis();
			                                                    }
																}});
		}
		
		function overShareThis(e:MouseEvent): void {
			flag_share = true;
			//share_this_mc.removeEventListener(MouseEvent.ROLL_OVER, overShareThis);
		}

		function removeShareThis(): void {
			flag_share = false;
			var child: MovieClip;
			while (share_this_mc.numChildren > 0) {
				child = MovieClip(share_this_mc.getChildAt(0));
				child.removeEventListener(MouseEvent.CLICK, bookmarkerClick);
				share_this_mc.removeChild(child);
			}
			TweenMax.to(share_this_mc, 0.2, {alpha:0, ease:Sine.easeOut, onComplete:function(){
			block.removeChild(share_this_mc);
							}});
		}
		
		function bookmarkerClick(e:MouseEvent):void {
			removeShareThis();
			for (var i:int = 0; i < bookmarks.length(); i++) {
				if (bookmarks[i].@name == e.target.name) {
					navigateToURL(new URLRequest(bookmarks[i].@link), "_blank");
				}
			}
			buildRequest("AddCounter","share",onUrlComplete);
		}

		function buildRequest(method: String, contentType: String, closure: Function): void {
            var url:String = counters_path;
            var newRequest:URLRequest = new URLRequest(url);

            var variables:URLVariables = new URLVariables();
            variables.method = method;
            variables.f = feature.@name;
            variables.c = contentType;
            newRequest.data = variables;
			
			var urlLoader: URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, closure);
			trace("request: contentType="+contentType+" method="+method+" feature="+feature.@name);
			urlLoader.load(newRequest);
		}

		function onUrlCompleteGetAll(e: Event) {
			//trace("request complete (data): result="+e.target.data.substr(1));
		}
		
		function onUrlCompleteGetLike(e: Event) {
			//details_white_line
			if (e.target.data.substr(1) == "ERROR") {
				block.viewgallery.txtLike.txt.text = "";
			}
			if (e.target.data.substr(1) != "OK") {
				//trace("request complete (like): result="+e.target.data.substr(1));
				block.viewgallery.txtLike.txt.text = e.target.data..substr(1) + " people like this.";
			} else {
            	buildRequest("getcounerdetails","like",onUrlCompleteGetLike);
			}
		}

		function onUrlComplete(e: Event) {
			//trace("request complete (data): data="+e.target.data.substr(1));
		}

		function btnLikeClick(e:MouseEvent):void {
			block.viewgallery.btnLike.removeEventListener(MouseEvent.CLICK, btnLikeClick);
			block.viewgallery.btnLike.removeEventListener(MouseEvent.MOUSE_OVER, redOver);
			block.viewgallery.btnLike.removeEventListener(MouseEvent.MOUSE_OUT, redOut);
			block.viewgallery.btnLike.visible = false;
			block.viewgallery.btnLike_disable.visible = true;
            buildRequest("AddCounter","like",onUrlCompleteGetLike);
        }

		function backClick(e:MouseEvent):void {
			TweenMax.to(details_mc, 0.3, {alpha:0, onComplete:function(){
						removeChild(details_mc);
                        removeChild(details_white_line);
			            stage.align=StageAlign.TOP_LEFT;
			            stage.scaleMode=StageScaleMode.NO_SCALE;
						addChild(gallery_mc);
			            gallery_mc.alpha = 1;
			            flipCards();
			            addChild(_scrollBar);
			            _scrollBar.revive();    
						}});
		}
		
		function flipCards():void{
			var colImages = imagesOfFeature.length;
			for (var f: int = 0; f < colImages; f++) {
					allFeaturesLoaded(f);
			}
		}

		function onOver(e:MouseEvent):void {
			if (e.target is Loader && e.target.name.substr(0,6)!=BOX_ID && e.target.name.substr(0,8)!=LABEL_ID) {
				var image:Loader=Loader(e.target);
				TweenMax.from(image,1, {colorMatrixFilter:{contrast:1.7, brightness:1.7, saturation:0.5}});
			}
		}
		function onOut(e:MouseEvent):void {
			if (e.target is Loader && e.target.name.substr(0,6)!=BOX_ID && e.target.name.substr(0,8)!=LABEL_ID) {
				var image:Loader=Loader(e.target);
				TweenMax.to(image,0.75, {colorMatrixFilter:{contrast:1, brightness:1, saturation:1}});
			}
		}
		
		function CreateRedTitle(redtitle:String, xx:int, yy:int): MovieClip {
			var redTitle_mc: MovieClip = new MovieClip();
            redTitle_mc.x = xx;
            redTitle_mc.y = yy;
			redTitle_mc.name = LABEL_ID+titles.length.toString();
			
			var _font : Font = new Swis721HvBT();
			var textFormat:TextFormat = new TextFormat(_font.fontName,9, 0xFFFFFF);
			var redImageTitle:TextField = new TextField();
			redImageTitle.embedFonts = true;
            redImageTitle.text = redtitle.toUpperCase();
			redImageTitle.autoSize = TextFieldAutoSize.LEFT;
			redImageTitle.setTextFormat (textFormat);
			redImageTitle.mouseEnabled = false;
			
			var temp:Sprite = new Sprite();
            temp.graphics.beginFill(0xFF3334);
            temp.graphics.drawRect(0, 0, redImageTitle.width, redImageTitle.height); 
            temp.graphics.endFill();
			temp.mouseEnabled = false;
            redTitle_mc.addChild(temp); 
			
			redTitle_mc.addChild(redImageTitle);
			return redTitle_mc;
		}
		
		function CreateTitle(txttitle:String, xx:int, yy:int):MovieClip {
			var imageTitle_mc: MovieClip = new MovieClip();
            imageTitle_mc.x = xx;
            imageTitle_mc.y = yy;
			imageTitle_mc.name = LABEL_ID+titles.length.toString();

			var _font : Font = new Swiss721BoldCondensedBT();
			var textFormat:TextFormat = new TextFormat(_font.fontName,28, 0xFFFFFF);
			textFormat.bold = true;
			var imageTitle:TextField = new TextField();
			imageTitle.embedFonts = true;
			imageTitle.text = txttitle.toUpperCase();
			imageTitle.autoSize = TextFieldAutoSize.LEFT;
			imageTitle.setTextFormat (textFormat);
			imageTitle.mouseEnabled = false;

            var bmpFilter:BitmapFilter;
			bmpFilter = new DropShadowFilter(1,131,0x000000,78,4,4,1,BitmapFilterQuality.MEDIUM);   
            imageTitle.filters = [bmpFilter];
			
			var redarrow:Arrow = new Arrow();
		    redarrow.x = imageTitle.textWidth + 3;
			redarrow.y = imageTitle.textHeight/2 - 1;
			imageTitle_mc.addChild(redarrow);

			imageTitle_mc.addChild(imageTitle);
			return imageTitle_mc;
		}
		
		function CreateSlogan(slogan:String, xx:int, yy:int):MovieClip {
			var sloganTitle_mc: MovieClip = new MovieClip();
            sloganTitle_mc.x = xx;
            sloganTitle_mc.y = yy;
			sloganTitle_mc.name = LABEL_ID+titles.length.toString();
			
			var _font : Font = new Swis721CnBTRom();
			var textFormat:TextFormat = new TextFormat(_font.fontName,20, 0xFFFFFF);
			var sloganTitle:TextField = new TextField();
			sloganTitle.embedFonts = true;
            sloganTitle.text = slogan;
			sloganTitle.autoSize = TextFieldAutoSize.LEFT;
			sloganTitle.setTextFormat (textFormat);
			sloganTitle.mouseEnabled = false;

			var bmpFilter:BitmapFilter;
			bmpFilter = new DropShadowFilter(1,131,0x000000,78,4,4,1,BitmapFilterQuality.MEDIUM);   
            sloganTitle.filters = [bmpFilter];

            var slogan_bg:Sprite = new Sprite();
            slogan_bg.graphics.beginFill(0x000000,0.55);  //second param controls alpha
            slogan_bg.graphics.drawRect(0, 0, sloganTitle.width, sloganTitle.height); 
            slogan_bg.graphics.endFill();
			slogan_bg.mouseEnabled = false;
            sloganTitle_mc.addChild(slogan_bg);

			sloganTitle_mc.addChild(sloganTitle);
			return sloganTitle_mc;
		}
	}
}