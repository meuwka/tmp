jQuery(document).ready(function($){

  //Page scroll
  new cbpScroller( document.getElementById( 'jsPageScroller' ) );

  //smooth scroll

  $('a[href^="#"]').on('click', function(event) {
    var target = $( $(this).attr('href') );
    if( target.length ) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 300);
    }
  });

  $('[data-href^="#"]').on('click', function(event) {
    var target = $( $(this).attr('data-href') );
    if( target.length ) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 300);
    }
  });

  //block Partners
  $('[data-slider="partners"]').slick({
    dots: true,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  //block Reviews
  $('[data-slider="reviews"]').slick({
    dots: true,
    arrows: true,
    adaptiveHeight: true,
    fade: true
  });

  //block Docs

  $('[data-slider="documents"]').slick({
    infinite: false,
    arrows: true,
    dots: true,
    adaptiveHeight: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  // slider bottom Docs in modal

  $('[data-js="documentsModal"]').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 9,
    slidesToScroll: 9,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 7
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  //Clock
  $('[data-js="flipClock"]').FlipClock(3000, {
    countdown: true,
    clockFace: 'MinuteCounter'
  });

  // Video
  new UIMorphingButton( document.querySelector( '.morph-button' ), {
    closeEl : '.morph-content__close'
  } );

  $('#jsVideoClose').on('click', function(event) {
    $("#jsVideoAbout").trigger('pause');
  });

  // Collapse

  $('[data-js="collapsePanel"]').hide();
  $('[data-js="collapse"]').first().toggleClass('active').next('[data-js="collapsePanel"]').first().show();

  $('[data-js="collapse"]').on('click', function(e) {
    e.preventDefault();
    var el = $(this);
    el.toggleClass('active');
    el.next('[data-js="collapsePanel"]').slideToggle(200);
  });

  //Click Document Item

  $('[data-js="collapsePanel"]').each(function() {
    var $this = $(this);
    $this.on('click', 'a', function(e) {
      e.preventDefault();
      $this.children().removeClass('active');
      $(this).parent().addClass('active');
      var screenSize = checkWindowWidth();
      if ( screenSize ) {
        toggleRemodalMobileNav();
      }
    });
  });

  //Back

  $('[data-js="remodalBack"]').on('click', function(e) {
    e.preventDefault();
    toggleRemodalMobileNav();
  });

  //modal scans

  var indexSelectedDoc = 0;
  var elScans = $('[data-slider="scans"]');
  var firstScansOpen = true;

  $('[data-remodal-target="scans"]').on('click', function(e) {
    var el = $(this);
    indexSelectedDoc = el.parent().index();

    if (firstScansOpen) {
      elScans.find('img').each(function() {
        var dataSrc = $(this).attr('data-src');
        $(this).attr('src', dataSrc).removeAttr('data-src');
      });
      firstScansOpen = false;
    }
  });

  $(document).on('opened', '[data-remodal-id="scans"]', function () {

    slickScans();

  });

  function slickScans() {
    elScans.slick({
      initialSlide: indexSelectedDoc,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      lazyLoad: 'ondemand',
      asNavFor: '[data-slider="documents"]'
    });
  }

  $(document).on('closing', '[data-remodal-id="scans"]', function () {
    elScans.slick('unslick');
  });

  //Cover animation

  var animatedCoverBg = (function() {
    var docElem = document.documentElement;
    var coverElem = document.querySelector( '.cover' );
    var didScroll = false;
    var changeCoverOn = document.querySelector('.cover__title').getBoundingClientRect().top + window.pageYOffset;

    function init() {
      if( Modernizr.touch ) return;
      window.addEventListener( 'scroll', function( e ) {
        if( !didScroll ) {
          didScroll = true;
          scrollPage();
        }
      }, false );
    }

    function scrollPage() {
      var sy = window.pageYOffset;
      if ( sy >= changeCoverOn ) {
        classie.add( coverElem, 'cover--animated' );
      }
      else {
        classie.remove( coverElem, 'cover--animated' );
      }
      didScroll = false;
    }

    function scrollY() {
      return window.pageYOffset || docElem.scrollTop;
    }

    init();

  })();

  //Functions

  function checkWindowWidth() {
    var mq = window.getComputedStyle(document.querySelector('body'), '::after').getPropertyValue('content').replace(/"/g, '').replace(/'/g, "");
    return ( mq == 'mobile' ) ? true : false;
  }

  function toggleRemodalMobileNav() {
    $('[data-remodal-id=documents]').toggleClass('nav-is-visible');
  }

  //Form analitics

  $('[data-action=analitics]').on('click', function(event) {
    yaCounter32058116.reachGoal('zayavka');
    ga('send', 'event', 'zayavka', 'otpr');
  });

  //Form submit
  var formRequest = $('#cForm');
  var formData = {
    'name' 		    : '',
    'email' 		: '',
    'tel' 	        : ''
  };
  var validInputFields = true;
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  var intRegex = /[0-9 -()+]+$/;

  formRequest.submit(function(e) {
    e.preventDefault();
    formData = {
      'name' : $('input[name=name]').val(),
      'email': $('input[name=email]').val(),
      'tel'  : $('input[name=tel]').val()
    };
    clearForm();
    if (validateForm()) {
      initRequest();
    }

  });

  function clearForm() {
    var validInputFields = true;
    $('.form__control').removeClass('error');
  }
  function validateForm() {
    var nameContact = formData['name'];
    var email = formData['email'];
    var phone = formData['tel'];
    if (nameContact.length < 3) {
      $('[data-input="name"]').addClass('error');
      return false;
    }
    if ((!emailReg.test(email)) || email == '') {
      $('[data-input="email"]').addClass('error');
      return false;
    }
    if ((phone.length < 6) || (!intRegex.test(phone))){
      $('[data-input="tel"]').addClass('error');
      return false;
    }
    return true;
  }
  function initRequest() {
    $.ajax({
      type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url 		: 'process.php', // the url where we want to POST
      data 		: formData, // our data object
      dataType 	: 'json', // what type of data do we expect back from the server
      encode    : true
    })
      // using the done promise callback
        .done(function(data) {
          formRequest[0].reset();
          $(".form__alert" ).remove();
          formRequest.append('<div class="form__alert">' + data.message + '</div>');
        })
  }

});
